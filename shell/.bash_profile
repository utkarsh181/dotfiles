#
# ~/.bash_profile
#

# Source bashrc.
[ -f "$HOME"/.bashrc ] && . "$HOME"/.bashrc

if [ -z $DISPLAY ] && [ "$(tty)" = "/dev/tty1" ]; then
    # NOTE 2021-09-21: Check my systemd units.  Those are meant to work
    # around the fact that Wayland does not read environment variables from
    # something like ~/.profile.
    export SSH_AUTH_SOCK=$XDG_RUNTIME_DIR/gnupg/S.gpg-agent.ssh

    export DESKTOP_SESSION="sway"
    exec sway
fi

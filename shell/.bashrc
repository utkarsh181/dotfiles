#
# ~/.bashrc
#

# Shorter version of a common command that it used herein.
_checkexec()
{
    command -v "$1" > /dev/null
}

# Import variable to user's systemd
_systemctl_import ()
{
    systemctl --user import-environment $1
}

_vterm_printf ()
{
    if [ -n "$TMUX" ] && ([ "${TERM%%-*}" = "tmux" ] || [ "${TERM%%-*}" = "screen" ]); then
        # Tell tmux to pass the escape sequences through
        printf "\ePtmux;\e\e]%s\007\e\\" "$1"
    elif [ "${TERM%%-*}" = "screen" ]; then
        # GNU screen (screen, screen-256color, screen-256color-bce)
        printf "\eP\e]%s\007\e\\" "$1"
    else
        printf "\e]%s\e\\" "$1"
    fi
}

_vterm_prompt_end ()
{
    _vterm_printf "51;A$(whoami)@$(hostname):$(pwd)"
}

# Define environment variable according to XDG specification.
if [ -d $HOME/.local/share ]; then
    # $XDG_DATA_HOME defines the base directory relative to which user-specific
    # data files should be stored.
    export XDG_DATA_HOME=$HOME/.local/share
fi
if [ -d $HOME/.config ]; then
    # $XDG_CONFIG_HOME defines the base directory relative to which
    # user-specific configuration files should be stored.
    export XDG_CONFIG_HOME=$HOME/.config
fi
if [ -d $HOME/.local/state ]; then
    # The $XDG_STATE_HOME contains state data that should persist between
    # (application) restarts, but that is not important or portable enough to
    # the user that it should be stored in $XDG_DATA_HOME. It may contain:

    # * actions history (logs, history, recently used files, ...)

    # * current state of the application that can be reused on a restart (view,
    #   layout, open files, undo history, ...)
    export XDG_STATE_HOME=$HOME/.local/state
fi
if [ -d $HOME/.local/bin ]; then
    # User-specific executable files may be stored in
    # $HOME/.local/bin. Distributions should ensure this directory shows up in
    # the UNIX $PATH environment variable, at an appropriate place.
    export PATH=$HOME/.local/bin:$PATH
fi
if [ -d $HOME/.cache ]; then
    # $XDG_CACHE_HOME defines the base directory relative to which user-specific
    # non-essential data files should be stored.
    export XDG_CACHE_HOME=$HOME/.cache
fi

# Default editor.
if _checkexec emacs; then
    export VISUAL="emacsclient -c -a emacs"
    export EDITOR="emacsclient -c -a emacs"
elif _checkexec gvim; then
    export VISUAL="gvim"
    export EDITOR=vim
else
    export VISUAL=vim
    export EDITOR=$VISUAL
fi

# Default pager.  Note that the option I pass to it will quit once you
# try to scroll past the end of the file.
if [ $TERM = 'dumb' ]; then
    export PAGER="cat"
else
    export PAGER="less --quit-at-eof"
fi

export MANPAGER="$PAGER"

# Simple prompt
if [ -n "$SSH_CONNECTION" ]; then
    export PS1="\u@\h: \w \$ "
else
    export PS1="\w \$ "
fi
export PS1=$PS1'\[$(_vterm_prompt_end)\]'
export PS2="> "

# For setting history length see HISTSIZE and HISTFILESIZE in `man bash`.
if [ -f $XDG_DATA_HOME/bash_history ]; then
    export HISTFILE=$XDG_DATA_HOME/bash_history
else
    touch $XDG_DATA_HOME/history
fi

HISTSIZE=1000
HISTFILESIZE=2000

# Enable automatic color support for common commands that list output
# and also add handy aliases.
alias diff='diff --color=auto'

alias dir='dir --color=auto'
alias vdir='vdir --color=auto'

alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

# Make ls a bit easier to read.  Note that the -A is the same as -a but
# does not include implied paths (the current dir denoted by a dot and
# the previous dir denoted by two dots).
alias ls='ls -pv --color=auto --group-directories-first'
alias lsa='ls -pvA --color=auto --group-directories-first'
alias lsl='ls -lhpv --color=auto --group-directories-first'
alias lsla='ls -lhpvA --color=auto --group-directories-first'

# Safer default for cp, mv, rm.  These will print a verbose output of
# the operations.  If an existing file is affected, they will ask for
# confirmation.  This can make things a bit more cumbersome, but is a
# generally safer option.
alias cp='cp -iv'
alias mv='mv -iv'
alias rm='rm -Iv'

if _checkexec stow; then
    alias stow="stow -v"
fi

# email indexer
if _checkexec notmuch; then
    export NOTMUCH_CONFIG=$XDG_CONFIG_HOME/notmuch-config
    _systemctl_import NOTMUCH_CONFIG # Used in notmuch.service.
fi

# password store
if _checkexec pass; then
    export PASSWORD_STORE_DIR=$XDG_DATA_HOME/password-store
    _systemctl_import PASSWORD_STORE_DIR # Used in notmuch.service.
fi

# remove less history
if _checkexec less; then
    export LESSHISTFILE=-
fi

# TeX Live directory
if [ -d /usr/local/texlive/2024 ] ; then
    export PATH=/usr/local/texlive/2024/bin/x86_64-linux:$PATH
    export INFOPATH=/usr/local/texlive/2024/texmf-dist/doc/info:$INFOPATH
    export MANPATH=/usr/local/texlive/2024/texmf-dist/doc/man:$MANPATH
fi

# GNU Octave directory
if _checkexec octave; then
    export OCTAVE_HISTFILE=$XDG_CACHE_HOME/octave-hsts
    export OCTAVE_SITE_INITFILE=$XDG_CONFIG_HOME/octave/octaverc
fi

# SQLite directory
if _checkexec sqlite3; then
    export SQLITE_HISTORY=$XDG_DATA_HOME/sqlite_history
fi

# Yarn directory
if _checkexec yarn; then
    alias yarn='yarn --use-yarnrc $XDG_CONFIG_HOME/yarn/config'
fi

# Racket directory
if _checkexec racket; then
    export PLTUSERHOME=$XDG_DATA_HOME/racket
fi

# NPM directory
if _checkexec npm; then
    export NPM_CONFIG_USERCONFIG=$XDG_CONFIG_HOME/npm/npmrc
fi

# Cargo directory
if _checkexec rust; then
    export CARGO_HOME=$XDG_DATA_HOME/cargo
fi

# Custom Texinfo files
if [ -d $XDG_DATA_HOME/info ]; then
    export INFOPATH=$XDG_DATA_HOME/info:/usr/share/info:$INFOPATH
fi

# Copyright update script
if _checkexec update-copyright; then
    export UPDATE_COPYRIGHT_HOLDER="Utkarsh Singh"
fi

# Please make sure this line is placed toward the end of the shell configuration
# file since it manipulates PATH during the initialization.
if _checkexec pyenv; then
    export PYENV_ROOT="$HOME/.pyenv"
    export PATH="$PYENV_ROOT/bin:$PATH"
    eval "$(pyenv init -)"
fi

# Load Angular CLI autocompletion.
source <(ng completion script)

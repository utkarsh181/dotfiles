;;; ut-eshell.el --- Extensions to Eshell for my dotemacs -*- lexical-binding: t -*-

;; Copyright (C) 2021, 2022, 2023 Utkarsh Singh

;; Author: Utkarsh Singh <utkarsh190601@gmail.com>
;; URL: https://gitlab.com/utkarsh181/dotfiles
;; Package-Requires: ((emacs "27.1"))

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; This covers my Eshell extensions, for use in my Emacs setup:
;; https://gitlab.com/utkarsh181/dotfiles

;;; Code:

(require 'eshell)
(require 'esh-mode)
(require 'em-dirs)
(require 'em-hist)

(defgroup ut-eshell ()
  "Extensions for Eshell and related libraries."
  :group 'shell)

(defvar ut-eshell--last-dirs '()
  "Minibuffer history for ut-eshell directories.")

(defun ut-eshell-clear-scrollback ()
  "Clear contents of eshell buffer."
  (interactive)
  (let ((eshell-buffer-maximum-lines 0))
    (eshell-truncate-buffer)))

(defun ut-eshell--dirs ()
  "Return completion table with dirs in `eshell-last-dir-ring'."
  (let ((dirs (ring-elements eshell-last-dir-ring)))
    (delete-dups (mapcar 'abbreviate-file-name dirs))))

(defun ut-eshell--dirs-prompt (dirs)
  "Helper of `ut-eshell-last-dir' to read DIRS."
  (let ((def (car ut-eshell--last-dirs)))
    (completing-read
     (format-prompt "Switch to dir" def)
     dirs nil t nil 'ut-eshell--last-dirs def)))

(defun ut-eshell-last-dir (dir &optional arg)
  "Switch to a DIR directory in Eshell using completion.
With optional ARG prefix argument (\\[universal-argument]) also
open the directory in a `dired' buffer."
  (interactive
   (list (ut-eshell--dirs-prompt (ut-eshell--dirs))
	 current-prefix-arg))
  (insert dir)
  (eshell-send-input)
  (when arg
    (dired dir)))

(provide 'ut-eshell)
;;; ut-eshell.el ends here

(use-package ut-publish
  :custom
  (ut-publish-sitemap-file "index.org")
  (ut-publish-base-directory "~/Documents/projects/website/blog")
  (ut-publish-publishing-directory "~/Documents/projects/website/blog")
  (ut-publish-site-url "https://utkarshsingh.xyz/blog")
  (ut-publish-css-file "../style.css")
  (ut-publish-feed-title "Utkarsh Singh's Blog")
  :config
  (setq org-publish-project-alist
	`(("website"
	   :base-directory ,ut-publish-base-directory
	   :publishing-directory ,ut-publish-publishing-directory
	   :publishing-function org-html-publish-to-html

	   :with-toc nil
	   :section-numbers nil
	   :html-head ,(ut-publish-html-head)

	   :auto-sitemap t
	   :sitemap-filename ,ut-publish-sitemap-file
	   :sitemap-title "Posts"
	   :sitemap-sort-files anti-chronologically

	   :html-preamble ,(ut-publish-file-contents "preamble.html")
	   :html-postamble ,(ut-publish-file-contents "postamble.html")
	   :completion-function ut-publish-completion))))

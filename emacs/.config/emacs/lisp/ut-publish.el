;;; ut-publish.el --- Extension to ox-publish.el     -*- lexical-binding: t; -*-

;; Copyright (C) 2021, 2022, 2023 Utkarsh Singh

;; Author: Utkarsh Singh <utkarsh190601@gmail.com>
;; Keywords: convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This library covers my extension to ox-publish.el with the help of Pierre
;; Neidhardt's webfeeder.el to publish my website: <https://utkarshsingh.xyz>.

;;; Code:

(when (featurep 'webfeeder)
  (require 'webfeeder))

(require 'ox-publish)
(require 'cl-lib)

(defgroup ut-publish ()
  "Personal extension to ox-publish.el."
  :group 'ox-publish)

(defcustom ut-publish-sitemap-file "sitemap.org"
  "Filename for output of sitemap."
  :type 'string
  :group 'ut-publish)

(defcustom ut-publish-css-file "style.css"
  "Filename for CSS used for published website."
  :type 'string
  :group 'ut-publish)

(defcustom ut-publish-feed-file "atom.xml"
  "Filename for RSS/Atom feed.

Used to keep track of newly published articles."
  :type 'string
  :group 'ut-publish)

(defcustom ut-publish-base-directory "~/org"
  "Directory containing publishing source files."
  :type 'directory
  :group 'ut-publish)

(defcustom ut-publish-publishing-directory "~/public_html"
  "Directory where output files are published."
  :type 'directory
  :group 'ut-publish)

(defcustom ut-publish-site-url "https://example.com"
  "URL of published website."
  :type 'string
  :group 'ut-publish)

(defcustom ut-publish-feed-title "Updates for example.com"
  "Title for published feed files.

Used in `webfeeder-build'."
  :type 'string
  :group 'ut-publish)

(defcustom ut-publish-feed-description ""
  "Description for published feed files.

Used in `webfeeder-build'."
  :type 'string
  :group 'ut-publish)

(defun ut-publish-html-files ()
  "Return list of HTML files in `ut-publish-publishing-directory'."
  (let ((ignored-file
	 (mapcar (lambda (f) (file-name-concat ut-publish-publishing-directory f))
		 '("index.html" "preamble.html" "postamble.html"))))
    (seq-filter
     (lambda (f1)
       (cl-notany (lambda (f2) (string-equal f1 f2)) ignored-file))
     (directory-files-recursively
      ut-publish-publishing-directory
      "\\.html\\'"))))

(defun ut-publish-html-head ()
  "Return HTML head for published post.

Also see `org-html-head'."
  (format
   "<link rel='stylesheet' type='text/css' href='%s'>"
   ut-publish-css-file))

(defun ut-publish-file-contents (filename)
  "Return contents of file FILENAME in `ut-publish-base-directory'."
  (with-temp-buffer
    (insert-file-contents
     (file-name-concat ut-publish-base-directory filename))
    (buffer-string)))

(declare-function webfeeder-build "webfeeder")

(defun ut-publish-completion (info)
  "Completion function for project property list INFO."
  (webfeeder-build
   ut-publish-feed-file
   ut-publish-publishing-directory
   ut-publish-site-url
   (ut-publish-html-files)
   :title ut-publish-feed-title
   :description ut-publish-feed-description))

(provide 'ut-publish)
;;; ut-publish.el ends here

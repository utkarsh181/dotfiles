;;; ut-recentf.el --- Extensions to recentf.el for my dotemacs -*- lexical-binding: t -*-

;; Copyright (c) 2021, 2022, 2023 Utkarsh Singh <utkarsh190601@gmail.com>

;; Author: Utkarsh Singh <utkarsh190601@gmail.com>
;; Package-Requires: ((emacs "27.1"))

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or (at
;; your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Extensions to `recentf.el' for my Emacs configuration:
;; <https://gitlab.com/utkarsh181/dotfiles>

;;; Code:

(require 'recentf)

(defun ut-recentf-keep-predicate (file)
  "Additional conditions for saving FILE in `recentf-list'.
Add this function to `recentf-keep'."
  (cond
   ((file-directory-p file) (file-readable-p file))))

(defvar ut-recentf--history-files '()
  "Minibuffer history for ut-recentf files.")

(defvar ut-recentf--history-dirs '()
  "Minibuffer history for ut-recentf directories.")

(defun ut-recentf--files ()
  "Return completion table with files in `recentf-list'."
  (mapcar 'abbreviate-file-name recentf-list))

(defun ut-recentf--files-prompt (files)
  "Helper of `ut-recentf-recent-files' to read FILES."
  (let ((def (car ut-recentf--history-files)))
    (completing-read
     (format-prompt "Recentf" def)
     files nil t nil 'ut-recentf--history-files def)))

(defun ut-recentf-recent-files (file &optional arg)
  "Select FILE from `recentf-list' using completion.

If \\[universal-argument] prefix is supplied the open file in
other window."
  (interactive
   (list (ut-recentf--files-prompt (ut-recentf--files))
	 current-prefix-arg))
  (if arg
      (find-file-other-window file)
    (find-file file))
  (add-to-history 'ut-recentf--history-files file))

(defun ut-recentf--dirs ()
  "Return completion table with directories in `recentf-list'."
  (let ((list (mapcar 'abbreviate-file-name recentf-list)))
    (mapcar (lambda (file)
              (if (file-directory-p file)
                  (directory-file-name file)
                (substring (file-name-directory file) 0 -1)))
            list)))

(defun ut-recentf--dirs-prompt (dirs)
  "Helper of `ut-recentf-recent-dirs' to read DIRS."
  (let ((def (car ut-recentf--history-dirs)))
    (completing-read
     (format-prompt "Recent dir" def)
     dirs nil t nil 'ut-recentf--history-dirs def)))

(defun ut-recentf-recent-dirs (dir)
  "Select DIR from `recentf-list' using completion."
  (interactive
   (list (ut-recentf--dirs-prompt (ut-recentf--dirs))))
  (find-file dir)
  (add-to-history 'ut-recentf--history-dirs dir))

(provide 'ut-recentf)
;;; ut-recentf.el ends here

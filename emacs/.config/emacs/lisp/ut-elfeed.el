;;; ut-elfeed.el --- Elfeed extensions for my dotemacs -*- lexical-binding: t -*-

;; Copyright (C) 2021, 2022, 2023 Utkarsh Singh <utkarsh190601@gmail.com>

;; Author: Utkarsh Singh <utkarsh190601@gmail.com>
;; Package-Requires: ((emacs "27.1"))

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Extensions for Elfeed, intended for use in my Emacs setup:
;; https://gitlab.com/utkarsh181/dotfiles

;;; Code:

(eval-when-compile (require 'subr-x))

(when (featurep 'elfeed)
  (require 'elfeed))

(defgroup ut-elfeed ()
  "Personal extension for Elfeed."
  :group 'elfeed)

(defcustom ut-elfeed-feeds-file (concat user-emacs-directory "feeds.gpg")
  "Name of the file that record `elfeed-feeds'."
  :type 'string
  :group 'ut-elfeed)

;; TODO: should this function be a part of some hook?
(defun ut-elfeed-feeds-to-file ()
  "Write `elfeed-feeds' to `ut-elfeed-feeds-file'."
  (let ((file (expand-file-name ut-elfeed-feeds-file))
	(conding-system-for-write 'utf-8))
    (with-current-buffer (get-buffer-create ut-elfeed--feeds-buffer)
      (delete-region (point-min) (point-max))
      (insert (format ";;; -*- coding: %s; mode: lisp-data -*-\n"
                      coding-system-for-write))
      (let ((print-length nil)
            (print-level nil))
        (pp elfeed-feeds (current-buffer)))
      (condition-case nil
	  ;; Don't use write-file; we don't want this buffer to visit it.
          (write-region (point-min) (point-max) file)
	(file-error (message "Elfeed feeds: can't write %s" file)))
      (kill-buffer (current-buffer)))))

(defvar ut-elfeed--feeds-loaded nil
  "Non-nil means that the `ut-elfeed-feeds-file' has been loaded.")

(defun ut-elfeed-load-feeds ()
  "Load file containing the `elfeed-feeds' list.
Add this to `elfeed-search-mode-hook'."
  (unless ut-elfeed--feeds-loaded
    (setq ut-elfeed--feeds-loaded t)
    (let ((file (expand-file-name ut-elfeed-feeds-file)))
      (when (file-readable-p file)
	(with-temp-buffer
	  (delete-region (point-min) (point-max))
	  (insert-file-contents file)
	  (goto-char (point-min))
	  (setq elfeed-feeds
		(car (read-from-string
		      (buffer-substring (point-min) (point-max))))))))))

(defun ut-elfeed-reload-feeds ()
  "Reload file containing the `elfeed-feeds' list."
  (interactive)
  (setq ut-elfeed--feeds-loaded t)
  (ut-elfeed-load-feeds))

(defun ut-elfeed-show-eww (&optional link)
  "Browse current entry's link or optional LINK in `eww'.

Only show the readable part once the website loads.  This can
fail on poorly-designed websites."
  (interactive)
  (let* ((entry (if (eq major-mode 'elfeed-show-mode)
                    elfeed-show-entry
                  (elfeed-search-selected :ignore-region)))
         (link (or link (elfeed-entry-link entry))))
    (eww link)
    (add-hook 'eww-after-render-hook 'eww-readable nil t)))

(defvar ut-elfeed-mpv-buffer-name "*ut-elfeed-mpv-output*"
  "Name of buffer holding Elfeed MPV output.")

(defun ut-elfeed--get-mpv-buffer ()
  "Prepare `ut-elfeed-mpv-buffer-name' buffer."
  (let ((buf (get-buffer ut-elfeed-mpv-buffer-name))
        (inhibit-read-only t))
    (with-current-buffer buf
      (erase-buffer))))

(declare-function elfeed-entry-enclosures "elfeed")

(defun ut-elfeed-mpv-dwim ()
  "Play entry link with the external MPV program."
  (interactive)
  (let* ((entry (if (eq major-mode 'elfeed-show-mode)
                    elfeed-show-entry
                  (elfeed-search-selected :ignore-region)))
         (link (elfeed-entry-link entry))
         buf)
    (elfeed-untag entry 'unread)
    (when (eq major-mode 'elfeed-search-mode)
      (elfeed-search-update-entry entry)
      (unless elfeed-search-remain-on-entry
	(forward-line)))
    (setq buf (pop-to-buffer ut-elfeed-mpv-buffer-name))
    (ut-elfeed--get-mpv-buffer)
    ;; TODO: do we need argument for mpv?
    (start-process "ut-elfeed-mpv" buf "mpv"
		   "--no-msg-color" link)
    (message "Launching MPV for %s" link)))

(defvar ut-elfeed-youtube-dl-buffer-name "*ut-elfeed-youtube-dl-output*"
  "Name of buffer holding Elfeed youtube-dl output.")

(defvar ut-elfeed-youtube-dl-dir "~/Downloads"
  "Download directory for youtube-dl.")

(defvar ut-elfeed-youtube-dl-switches "-f best"
  "Command-line arguments to pass to youtube-dl process.")

(defun ut-elfeed--get-youtube-dl-buffer ()
  "Prepare `ut-elfeed-youtube-dl-buffer-name' buffer."
  (let ((buf (get-buffer ut-elfeed-youtube-dl-buffer-name))
        (inhibit-read-only t))
    (with-current-buffer buf
      (erase-buffer))))

(defun ut-elfeed-youtube-dl-dwim ()
  "Download entry linke with external youtube-dl program."
  (interactive)
  (let* ((entry (if (eq major-mode 'elfeed-show-mode)
                    elfeed-show-entry
                  (elfeed-search-selected :ignore-region)))
	 (link (elfeed-entry-link entry))
	 (default-directory (expand-file-name ut-elfeed-youtube-dl-dir))
         buf)
    (elfeed-untag entry 'unread)
    (when (eq major-mode 'elfeed-search-mode)
      (elfeed-search-update-entry entry)
      (unless elfeed-search-remain-on-entry
	(forward-line)))
    (setq buf (pop-to-buffer ut-elfeed-youtube-dl-buffer-name))
    (ut-elfeed--get-youtube-dl-buffer)
    ;; TODO: find a better method the pass arguments
    (start-process "ut-elfeed-youtube-dl" buf "youtube-dl"
		   "--no-color" ut-elfeed-youtube-dl-switches link)
    (message "Downloading %s with youtube-dl" link)))

(provide 'ut-elfeed)
;;; ut-elfeed ends here

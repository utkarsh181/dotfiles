;;; ut-dired.el --- Extensions to dired.el for my dotemacs -*- lexical-binding: t -*-

;; Copyright (C) 2021, 2022, 2023 Utkarsh Singh

;; Author: Utkarsh Singh <utkarsh190601@gmail.com>
;; URL: https://gitlab.com/utkarsh181/dotfiles
;; Version: 0.1.0
;; Package-Requires: ((emacs "28.1"))

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; This covers my dired.el extensions, for use in my Emacs setup:
;; <https://gitlab.com/utkarsh181/dotfiles>.

;;; Code:

(require 'dired)
(require 'dired-x)

(defgroup ut-dired ()
  "Extensions for Dired."
  :group 'dired)

;;;; File associations

(defcustom ut-dired-media-extensions
  "\\.\\(mp[34]\\|ogg\\|flac\\|webm\\|mkv\\)"
  "Regular expression for media file extensions.

Also see the function `ut-dired-media-player' and the variable
`ut-dired-media-players'.

To be used in user configurations while setting up the variable
`dired-guess-shell-alist-user'."
  :type 'string
  :group 'ut-dired)

(defcustom ut-dired-image-extensions
  "\\.\\(png\\|jpe?g\\|tiff\\)"
  "Regular expression for media file extensions.

Also see the function `ut-dired-image-viewer' and the variable
`ut-dired-image-viewers'.

To be used in user configurations while setting up the variable
`dired-guess-shell-alist-user'."
  :type 'string
  :group 'ut-dired)

(defcustom ut-dired-office-extensions
  "\\.\\(docx?\\|odt\\|pptx?\\|odp\\|xlsx?\\|ods\\)"
  "Regular expression for Office suite file extensions.

To be used in user configurations while setting up the variable
`dired-guess-shell-alist-user'."
  :type 'string
  :group 'ut-dired)

(defcustom ut-dired-pdf-extensions
  "\\.pdf"
  "Regular expression for PDF file extensions.

To be used in user configurations while setting up the variable
`dired-guess-shell-alist-user'."
  :type 'string
  :group 'ut-dired)

(defcustom ut-dired-media-players '("mpv" "vlc" "totem")
  "List of strings for media player programs.

Also see the function `ut-dired-media-player' and the variable
`ut-dired-media-extensions'.

To be used in user configurations while setting up the variable
`dired-guess-shell-alist-user'."
  :type '(repeat string)
  :group 'ut-dired)

(defcustom ut-dired-image-viewers '("sxiv" "feh" "loupe")
  "List of strings for image viewer programs.

Also see the function `ut-dired-image-viewer' and the variable
`ut-dired-image-extensions'.

To be used in user configurations while setting up the variable
`dired-guess-shell-alist-user'."
  :type '(repeat string)
  :group 'ut-dired)

(defcustom ut-dired-office-suites '("libreoffice")
  "List of strings for Office suite programs.

Also see the function `ut-dired-office-suite' and the variable
`ut-dired-office-extensions'.

To be used in user configurations while setting up the variable
`dired-guess-shell-alist-user'."
  :type '(repeat string)
  :group 'ut-dired)

(defcustom ut-dired-pdf-viewers '("evince" "firefox")
  "List of strings for PDF viewers.

Also see the function `ut-dired-office-suite' and the variable
`ut-dired-office-extensions'.

To be used in user configurations while setting up the variable
`dired-guess-shell-alist-user'."
  :type '(repeat string)
  :group 'ut-dired)

(defmacro ut-dired-file-association (name programs)
  "Make NAME function to check for PROGRAMS."
  (declare (indent defun))
  `(defun ,name ()
     ,(format "Return available program.

Checks each entry in `%s' and returns the first program that
is available on the system.  If none is present, it falls back to
xdg-open (for GNU/Linux only).

This function is for use in `dired-guess-shell-alist-user'."
              programs)
     (catch :found
       (dolist (p (append ,programs '("xdg-open")))
	 (when (executable-find p)
	   (throw :found p))))))

(ut-dired-file-association
  ut-dired-media-player
  ut-dired-media-players)

(ut-dired-file-association
  ut-dired-image-viewer
  ut-dired-image-viewers)

(ut-dired-file-association
  ut-dired-office-suite
  ut-dired-office-suites)

(ut-dired-file-association
  ut-dired-pdf-viewer
  ut-dired-pdf-viewers)

(provide 'ut-dired)
;;; ut-dired.el ends here

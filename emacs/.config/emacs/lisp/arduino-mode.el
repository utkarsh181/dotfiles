;;; Arduino-mode.el --- Arduino Major Mode            -*- lexical-binding: t; -*-

;; Copyright (C) 2008  Christopher Grim

;; Author: Christopher Grim <christopher.grim@gmail.com>
;; Keywords: languages, arduino
;; Version: 1.0

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Commentary:
;;
;; Based on derived-mode-ex.el found here:
;;
;; <http://cc-mode.sourceforge.net/derived-mode-ex.el>.
;;

;;; Code:

(require 'cc-mode)

(eval-when-compile
  (require 'cc-langs)
  (require 'cc-fonts)
  (require 'cc-menus))

(eval-and-compile
  ;; fall back on c-mode
  (c-add-language 'arduino-mode 'c-mode))

(c-lang-defconst c-primitive-type-kwds
  arduino (append '("boolean" "byte")
                  (c-lang-const c-primitive-type-kwds)))

(c-lang-defconst c-constant-kwds
  arduino (append
           '("HIGH" "LOW"
             "INPUT" "OUTPUT" "INPUT_PULLUP"
             "LED_BUILTIN"
             "true" "false")
           (c-lang-const c-constant-kwds)))

(c-lang-defconst c-simple-stmt-kwds
  arduino (append
           '(;; Digital I/O
             "pinMode"
             "digitalWrite"
             "digitalRead"
             ;; Analog I/O
             "analogReference"
             "analogRead"
             "analogWrite"
             ;; Due only
             "analogReadResolution"
             "analogWriteResolution"
             ;; Advanced I/O
             "tone"
             "noTone"
             "shiftOut"
             "shiftIn"
             "pulseIn"
             ;; Time
             "millis"
             "micros"
             "delay"
             "delayMicroseconds"
             ;; Math
             "min"
             "max"
             "abs"
             "constrain"
             "map"
             "pow"
             "sqrt"
             ;; Trigonometry
             "sin"
             "cos"
             "tan"
             ;; Random Numbers
             "randomSeed"
             "random"
             ;; Bits and Bytes
             "lowByte"
             "highByte"
             "bitRead"
             "bitWrite"
             "bitSet"
             "bitClear"
             "bit"
             ;; External Interrupts
             "attachInterrupt"
             "detachInterrupt"
             ;; Interrupts
             "interrupts"
             "noInterrupts")
           (c-lang-const c-simple-stmt-kwds)))

(c-lang-defconst c-primary-expr-kwds
  arduino (append
           '(;; Communication
             "Serial"
             ;; USB (Leonoardo based boards and Due only)
             "Keyboard"
             "Mouse")
           (c-lang-const c-primary-expr-kwds)))

(defgroup arduino nil "Arduino mode customizations"
  :group 'languages)

(defcustom arduino-font-lock-extra-types nil
  "*List of extra types to recognize in Arduino mode.
Each list item should be a regexp matching a single identifier."
  :type 'c-extra-types-widget)

(defcustom arduino-executable "arduino-cli"
  "Arduino executable."
  :type 'string)

(defcustom arduino-fqbn nil
  "The FQBN for the connected board."
  :type 'string)

(defcustom arduino-port nil
  "The port that the board is connected to."
  :type 'string)

(defcustom arduino-sketch-dir nil
  "The directory that the sketch is in."
  :type 'directory)

(defcustom arduino-custom-lib-dir nil
  "A directory or list of directory specifying custom library directories."
  :type '(choice directory (repeat directory)))

(defconst arduino-font-lock-keywords-1 (c-lang-const c-matchers-1 arduino)
  "Minimal highlighting for Arduino mode.")

(defconst arduino-font-lock-keywords-2 (c-lang-const c-matchers-2 arduino)
  "Fast normal highlighting for Arduino mode.")

(defconst arduino-font-lock-keywords-3 (c-lang-const c-matchers-3 arduino)
  "Accurate normal highlighting for Arduino mode.")

(defvar arduino-font-lock-keywords arduino-font-lock-keywords-3
  "Default expressions to highlight in ARDUINO mode.")

(defvar arduino-mode-syntax-table nil
  "Syntax table used in arduino-mode buffers.")

(or arduino-mode-syntax-table
    (setq arduino-mode-syntax-table
          (funcall (c-lang-const c-make-mode-syntax-table arduino))))

(defvar arduino-mode-abbrev-table nil
  "Abbreviation table used in arduino-mode buffers.")

(c-define-abbrev-table 'arduino-mode-abbrev-table
  ;; Keywords that if they occur first on a line might alter the
  ;; syntactic context, and which therefore should trigger
  ;; reindentation when they are completed.
  '(("else" "else" c-electric-continued-statement 0)
    ("while" "while" c-electric-continued-statement 0)))

(defun arduino-update ()
  "Get FQBN of the current device.
This function lags so use it in async.
Returns an alist of ((fqbn . fqbn) (port . port))
You can check it out in terminal by \"arduino-cli board list\"."
  (let ((result (shell-command-to-string (concat arduino-executable " board list"))))
    (string-match
     "Board Name +?\n\\([^ \t]+\\)[ \t]*\\([^ \t]+\\)"
     result)
    (ignore-errors
      `((fqbn . ,(setq arduino-fqbn (match-string 1 result)))
        (port . ,(setq arduino-port (match-string 2 result)))))))

(defun arduino--quote-string (str)
  "Quote % sign in STR."
  (replace-regexp-in-string "%" "%%" str t t))

(defun arduino--separate-lib ()
  "Return comma separated `arduino-custom-lib-dir'."
  (if (stringp arduino-custom-lib-dir)
      arduino-custom-lib-dir
    (string-join arduino-custom-lib-dir ",")))

(defun arduino--build-command (command sketch &optional port)
  "Add port and FQBN to command."
  (let (info)
    (unless (or arduino-fqbn arduino-port)
      (message "Collecting board information...")
      (setq info (arduino-update)))
    (format "%s %s %s %s --fqbn %s %s"
            arduino-executable
            command
            (if port
                (concat "--port " (or arduino-port (alist-get 'port info)))
              "")
	    (if (and (string-equal command "compile") arduino-custom-lib-dir)
		(concat "--libraries " (arduino--separate-lib))
	      "")
            (or arduino-fqbn (alist-get 'fqbn info))
            sketch)))

(defun arduino-compile-sketch ()
  "Compile current sketch."
  (interactive)
  (message "Compiling...")
  (message
   (arduino--quote-string
    (shell-command-to-string
     (arduino--build-command
      "compile"
      (or arduino-sketch-dir default-directory))))))

(defun arduino-upload-sketch ()
  "Upload current sketch."
  (interactive)
  (message "Uploading...")
  (message
   (arduino--quote-string
    (shell-command-to-string
     (arduino--build-command
      "upload"
      (or arduino-sketch-dir default-directory)
      t)))))

(defvar arduino-mode-map
  (let ((map (c-make-inherited-keymap)))
    map)
  "Keymap used in arduino-mode buffers.")

;;;###autoload
(add-to-list 'auto-mode-alist '("\\.pde\\'" . arduino-mode))
;;;###autoload
(add-to-list 'auto-mode-alist '("\\.ino\\'" . arduino-mode))

;;;###autoload
(define-derived-mode arduino-mode prog-mode "Arduino"
  "Major mode for editing Arduino code.

The hook `c-mode-common-hook' is run with no args at mode
initialization, then `arduino-mode-hook'.

Key bindings:
\\{arduino-mode-map}"
  (kill-all-local-variables)
  (c-initialize-cc-mode t)
  (set-syntax-table arduino-mode-syntax-table)
  (setq major-mode 'arduino-mode
        mode-name "Arduino"
        local-abbrev-table arduino-mode-abbrev-table
        abbrev-mode t
        imenu-generic-expression cc-imenu-c-generic-expression)
  (use-local-map c-mode-map)
  ;; `c-init-language-vars' is a macro that is expanded at compile
  ;; time to a large `setq' with all the language variables and their
  ;; customized values for our language.
  (c-init-language-vars arduino-mode)
  ;; `c-common-init' initializes most of the components of a CC Mode
  ;; buffer, including setup of the mode menu, font-lock, etc.
  ;; There's also a lower level routine `c-basic-common-init' that
  ;; only makes the necessary initialization to get the syntactic
  ;; analysis and similar things working.
  (c-common-init 'arduino-mode)
  (run-hooks 'c-mode-common-hook)
  (run-hooks 'arduino-mode-hook)
  (c-update-modeline))

(provide 'arduino-mode)
;;; arduino-mode.el ends here

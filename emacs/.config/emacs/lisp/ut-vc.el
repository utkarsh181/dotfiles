;;; ut-vc.el --- Extensions to vc.el for my dotemacs -*- lexical-binding: t -*-

;; Copyright (C) 2021, 2022, 2023 Utkarsh Singh

;; Author: Utkarsh Singh <utkarsh190601@gmail.com>
;; URL: https://gitlab.com/utkarsh181/dotfiles
;; Version: 0.1.0
;; Package-Requires: ((emacs "28.1"))

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; This covers my vc.el extensions, which mostly concern Git.  For use in my
;; Emacs setup: <https://gitlab.com/utkarsh181/dotfiles>.

;;; Code:

(require 'vc)
(require 'vc-git)
(require 'log-edit)

;;;; Customization options

(defgroup ut-vc ()
  "Extensions for vc.el and related libraries."
  :group 'project)

;;;; Command and helper function

;;;###autoload
(defun ut-vc-git-log-edit-extract-file-name (&optional arg)
  "Insert at point shortname from file in log edit buffers.

If multiple files are part of the log, a minibuffer completion
prompt will be produced: it can be used to narrow down to an
existing item or input an arbitrary string of characters.

With optional prefix ARG (\\[universal-argument]) remove
extension from file name."
  (interactive "P")
  (unless (derived-mode-p 'log-edit-mode)
    (user-error "Only try this in Log Edit mode"))
  (let* ((files (log-edit-files))
         (file (if (> (length files) 1)
                   (completing-read "Derive shortname from: " files nil nil)
                 (car files)))
         (name (file-name-nondirectory
                file)))
    (when arg
	(setq name (file-name-sans-extension name)))
    (insert (concat name ": "))))

;;;; User-interface setup

(defvar ut-vc--current-window-configuration nil
  "Current window configuration for use with Log Edit.")

(defvar ut-vc--current-window-configuration-point nil
  "Point in current window configuration for use with Log Edit.")

(defun ut-vc--store-window-configuration ()
  "Store window configuration before calling `vc-start-logentry'.
This should be called via `ut-vc-git-pre-log-edit-hook'."
  (setq ut-vc--current-window-configuration (current-window-configuration))
  (setq ut-vc--current-window-configuration-point (point)))

(defvar ut-vc-git-pre-log-edit-hook nil
  "Hook that runs right before `vc-start-logentry'.")

(defun ut-vc-git-pre-log-edit (&rest _)
  "Run `ut-vc-git-pre-log-edit-hook'.
To be used as advice before `vc-start-logentry'."
  (run-hooks 'ut-vc-git-pre-log-edit-hook))

(defun ut-vc--log-edit-restore-window-configuration ()
  "Set window configuration to the pre Log Edit state."
  (when ut-vc--current-window-configuration
    (set-window-configuration ut-vc--current-window-configuration))
  (when ut-vc--current-window-configuration-point
    (goto-char ut-vc--current-window-configuration-point)))

(defun ut-vc--log-edit-diff-window-configuration ()
  "Show current diff for Git Log Edit buffer."
  (let ((buffer (get-buffer "*vc-log*")))
    (with-current-buffer (if (buffer-live-p buffer)
                             buffer
                           (window-buffer (get-mru-window)))
      (delete-other-windows)
      (when (ignore-errors ; This condition saves us from error on new repos
              (process-lines "git" "--no-pager" "diff-index" "-p" "HEAD" "--"))
        (log-edit-show-diff))
      (other-window -1))))

(defun ut-vc--kill-log-edit ()
  "Local hook to restore windows when Log Edit buffer is killed."
  (when (or (derived-mode-p 'log-edit-mode)
            (derived-mode-p 'diff-mode))
    (add-hook 'kill-buffer-hook #'ut-vc--log-edit-restore-window-configuration 0 t)))

(defvar ut-vc-git-log-edit-done-hook nil
  "Hook that runs after `ut-vc-git-log-edit-done'.")

(define-minor-mode ut-vc-git-setup-mode
  "Extend `vc-git' Log View and Log Edit buffers.

Make Log Edit window configurations split between the message
composition buffer and the corresponding diff view: the previous window
configuration is restored upon the successful conclusion of the commit
or its termination by means of `log-edit-kill-buffer'."
  :init-value nil
  :global t
  (if ut-vc-git-setup-mode
      (progn
	(remove-hook 'log-edit-hook #'log-edit-show-files)
        (advice-add #'vc-start-logentry :before #'ut-vc-git-pre-log-edit)
        (add-hook 'ut-vc-git-pre-log-edit-hook #'ut-vc--store-window-configuration)
        (add-hook 'log-edit-mode-hook #'ut-vc--kill-log-edit)
        (add-hook 'ut-vc-git-log-edit-done-hook #'ut-vc--log-edit-restore-window-configuration)
        (add-hook 'log-edit-hook #'ut-vc--log-edit-diff-window-configuration))
    (add-hook 'log-edit-hook #'log-edit-show-files)
    (advice-remove #'vc-start-logentry #'ut-vc-git-pre-log-edit)
    (remove-hook 'ut-vc-git-pre-log-edit-hook #'ut-vc--store-window-configuration)
    (remove-hook 'log-edit-mode-hook #'ut-vc--kill-log-edit)
    (remove-hook 'ut-vc-git-log-edit-done-hook #'ut-vc--log-edit-restore-window-configuration)
    (remove-hook 'log-edit-hook #'ut-vc--log-edit-diff-window-configuration)))

(provide 'ut-vc)

;;; ut-vc.el ends here

;;; ut-eww.el --- Extensions to EWW for my dotemacs -*- lexical-binding: t -*-

;; Copyright (C) 2021, 2022, 2023 Utkarsh Singh

;; Author: Utkarsh Singh <utkarsh190601@gmail.com>
;; URL: https://gitlab.com/utkarsh181/dotfiles
;; Package-Requires: ((emacs "27.1"))

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; This covers my EWW extensions, for use in my Emacs setup:
;; <https://gitlab.com/utkarsh181/dotfiles>

;;; Code:

(require 'eww)
(require 'dired)

(defgroup ut-eww ()
  "Tweaks for EWW."
  :group 'eww)

;;;; Basic setup

(defvar ut-eww-visited-history nil
  "History of visited URLs.")

(defun ut-eww--record-history ()
  "Store URL in `ut-eww-visited-history'.
To be used by `eww-after-render-hook'."
  (let ((url (plist-get eww-data :url)))
    (add-to-history 'ut-eww-visited-history url)))

(add-hook 'eww-after-render-hook #'ut-eww--record-history)
(advice-add 'eww-back-url :after #'ut-eww--record-history)
(advice-add 'eww-forward-url :after #'ut-eww--record-history)

;;;; Commands

(defun ut-eww-browse-dwim (url &optional arg)
  "Visit a URL, maybe from `eww-prompt-history', with completion.

With optional prefix ARG (\\[universal-argument]) open URL in a
new eww buffer.

If URL does not look like a valid link, run a web query using
`eww-search-prefix'.

When called from an eww buffer, provide the current link as
initial input."
  (interactive
   (list
    (completing-read "Run EWW on: " (append ut-eww-visited-history eww-prompt-history)
                     nil nil (plist-get eww-data :url) 'eww-prompt-history)
    (prefix-numeric-value current-prefix-arg)))
  (eww url arg))

(defun ut-eww-visit-bookmark (&optional arg)
  "Visit bookmarked URL.

With optional prefix ARG (\\[universal-argument]) open URL in a
new EWW buffer."
  (interactive "p")
  (let (lst)
    (dolist (bookmark eww-bookmarks)
      (push (plist-get bookmark :url) lst))
    (eww (completing-read "Visit bookmark: " lst) arg)))

(defvar ut-eww-search-engine-alist
  '((debbugs . ut-eww-search-debbugs)
    (Wikipedia . ut-eww-search-wikipedia)
    (ArchWiki . ut-eww-search-arch-wiki)
    (AUR . ut-eww-search-arch-aur)
    (PythonDoc . ut-eww-search-python-doc))
  "Alist of web search commands.
The car of each cons cell is an arbitrary string that describes
the function it is associated with.")

(defvar ut-eww--engine-hist '()
  "Input history for `ut-eww-search-engine'.")

(defun ut-eww-search-engine (engine)
  "Use ENGINE stored in `ut-eww-search-engine-alist'."
  (interactive
   (list
    (completing-read
     "Search with: "
     (mapcar (lambda (x) (format "%s" (car x))) ut-eww-search-engine-alist) nil t
     nil 'ut-eww--engine-hist)))
  (call-interactively (alist-get (intern engine) ut-eww-search-engine-alist))
  (add-to-history 'ut-eww--engine-hist engine))

(defvar ut-eww--debbugs-hist '()
  "Input history for `ut-eww-search-debbugs'.")

(defun ut-eww-search-debbugs (number &optional arg)
  "Visit bug report NUMBER on debbugs.gnu.org.

With optional prefix ARG (\\[universal-argument]) open URL in a
new EWW buffer."
  (interactive
   (list (read-number "Browse debbugs number: " nil 'ut-eww--debbugs-hist)
         (prefix-numeric-value current-prefix-arg)))
  (eww
   (format "https://debbugs.gnu.org/cgi/bugreport.cgi?bug=%d" number)
   arg)
  (add-to-history 'ut-eww--debbugs-hist number))

(defvar ut-eww--wikipedia-hist '()
  "Input history for `ut-eww-search-wikipedia'.")

(defun ut-eww-search-wikipedia (string &optional arg)
  "Search Wikipedia page matching STRING.

With optional prefix ARG (\\[universal-argument]) open URL in a
new EWW buffer."
  (interactive
   (list (read-string "Search Wikipedia: " nil 'ut-eww--wikipedia-hist)
         (prefix-numeric-value current-prefix-arg)))
  (eww
   (format "https://en.m.wikipedia.org/w/index.php?search=%s" string)
   arg)
  (add-to-history 'ut-eww--wikipedia-hist string))

(defvar ut-eww--arch-wiki-hist '()
  "Input history for `ut-eww-search-arch-wiki'.")

(defun ut-eww-search-arch-wiki (string &optional arg)
  "Search Arch Linux Wiki page matching STRING.

With optional prefix ARG (\\[universal-argument]) open URL in a
new EWW buffer."
  (interactive
   (list (read-string "Search Arch Linux Wiki: " nil 'ut-eww--arch-wiki-hist)
         (prefix-numeric-value current-prefix-arg)))
  (eww
   (format "https://wiki.archlinux.org/index.php?search=%s" string)
   arg)
  (add-to-history 'ut-eww--arch-wiki-hist string))

(defvar ut-eww--arch-aur-hist '()
  "Input history for `ut-eww-search-arch-aur'.")

(defun ut-eww-search-arch-aur (string &optional arg)
  "Search Arch User Repository page matching STRING.

With optional prefix ARG (\\[universal-argument]) open URL in a
new EWW buffer."
  (interactive
   (list (read-string "Search Arch User Repository: " nil 'ut-eww--arch-aur-hist)
         (prefix-numeric-value current-prefix-arg)))
  (eww
   (format "https://aur.archlinux.org/packages/?K=%s" (string-replace " " "+" string))
   arg)
  (add-to-history 'ut-eww--arch-aur-hist string))

(defvar ut-eww--python-doc-hist '()
  "Input history for `ut-eww-search-python-doc'.")

(defun ut-eww-search-python-doc (string &optional arg)
  "Search Python documentation page matching STRING.

With optional prefix ARG (\\[universal-argument]) open URL in a
new EWW buffer."
  (interactive
   (list (read-string "Search Python Documentation: " nil 'ut-eww--arch-wiki-hist)
         (prefix-numeric-value current-prefix-arg)))
  (eww
   (format "https://www.python.org/search/?submit=&q=++++++++++++%s" (string-replace " " "+" string))
   arg)
  (add-to-history 'ut-eww--python-doc-hist string))

(defun ut-eww-dired-open-files (files)
  "Render marked FILES using EWW.

When called interactively pass file using `dired-get-marked-files'
as argument."
  (interactive (list (dired-get-marked-files)))
  (mapcar #'eww-open-file files))

(defun ut-eww-readable ()
  "Use more opinionated `eww-readable'.

Set width is set to `current-fill-column'.  Adjust size of
images."
  (interactive)
  (let ((shr-width (current-fill-column))
        (shr-max-image-proportion 0.35))
    (eww-readable)))

(provide 'ut-eww)
;;; ut-eww.el ends here

;;; ut-spell.el --- Spelling related extensions for my dotemacs -*- lexical-binding: t -*-

;; Copyright (C) 2021, 2022, 2023 Utkarsh Singh

;; Author: Utkarsh Singh <utkarsh190601@gmail.com>
;; URL: https://gitlab.com/utkarsh181/dotfiles
;; Version: 0.1.0
;; Package-Requires: ((emacs "28.1"))

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; This covers my spelling-related extensions, for use in my Emacs
;; setup: <https://gitlab.com/utkarsh181/dotfiles>

;;; Code:

;;; TODO: can we have an alternatve interface for choosing spelling?

(defgroup ut-spell ()
  "Extension of `ispell' and `flyspell'."
  :group 'ispell)

(defun ut-spell-spell-dwim ()
  "Spellcheck between BEG END, current word, or select dictionary."
  (interactive)
  (cond
   ((use-region-p)
    (let ((beg (region-beginning))
	  (end (region-end)))
      (cond
       ((derived-mode-p 'prog-mode)
	(ispell-comments-and-strings beg end))
       (t (ispell-region beg end)))))
   ((thing-at-point 'word)
    (call-interactively #'ispell-word))
   (t (call-interactively #'ispell-change-dictionary))))

(provide 'ut-spell)

;;; ut-spell.el ends here

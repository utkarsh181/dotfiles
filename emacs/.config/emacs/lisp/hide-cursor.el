;;; hide-cursor.el --- Make Emacs a pager             -*- lexical-binding: t; -*-

;; Copyright (C) 2022, 2023 Utkarsh Singh

;; Author: Utkarsh Singh <utkarsh190601@gmail.com>

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This package provides a pager-like experience inside Emacs.  A pager, like
;; Unix `less', is a program that provides a convenient reading environment by:
;;
;; 1. Hiding the cursor.
;; 2. Scrolling the view instead of the cursor.
;;
;; Based on Karthinks' blog: <https://karthinks.com/software/more-less-emacs/>.

;;; Code:

(defvar-local hide-cursor--original nil
  "Internal variable to record `cursor-type'.")

(define-minor-mode hide-cursor-mode
  "Hide or show the cursor.

When the cursor is hidden `scroll-lock-mode' is enabled, so that
the buffer works like a pager."
  :global nil
  :lighter "H"
  (if hide-cursor-mode
      (progn
        (scroll-lock-mode 1)
        (setq-local hide-cursor--original cursor-type)
        (setq-local cursor-type nil))
    (scroll-lock-mode -1)
    (setq-local cursor-type (or hide-cursor--original t))))

(provide 'hide-cursor)
;;; hide-cursor.el ends here

;;; emount.el --- Mount and umount usb and android from Emacs! -*- lexical-binding:t -*-

;; Copyright (C) 2021, 2022, 2023 Utkarsh Singh

;; Author: Utkarsh Singh <utkarsh190601@gmail.com>
;; Package-Requires: ((emacs "27.1"))

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or (at
;; your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(require 'ut-common)
(require 'subr-x)

(defvar emount--success-code 0
  "Exit code returned by programs on successful evaluation.")

(defun emount--select-mount-point ()
  "Select mount point and create if doesn't exist."
  (let (mount-point)
    (setq mount-point (read-directory-name "Type mount point: "))
    ;; if mount-point doesn't exit create it
    (unless (file-directory-p mount-point)
      (mkdir mount-point t))
    mount-point))

(defvar emount--usb-drive
  "lsblk -rpo 'name,type,size,mountpoint' | grep part"
  "Shell command to get usb drive names.")

(defun emount--get-drive-list (drive-cmd)
  "Run DRIVE-CMD and store result as a list."
  (split-string
   (shell-command-to-string drive-cmd) "\n" 'omit-nill))

(defun emount--select-unmounted-usb (usb-drive-list)
  "Select a unmounted usb drive from USB-DRIVE-LIST."
  (let (unmount-drive drive-info)
    (dolist (drive usb-drive-list)
      ;; drive-info => (name type size mount-point)
      (setq drive-info (split-string drive " " 'omit-nill))
      ;; if drive-info's mount-point is nil
      ;; capture drive's name
      (unless (nth 3 drive-info)
	(setq unmount-drive
	      (cons (nth 0 drive-info) unmount-drive))))
    (completing-read "Mount which drive: " unmount-drive)))

(defun emount-mount-usb (usb-name mount-point)
  "Mount usb with USB-NAME at MOUNT-POINT."
  (interactive (list (emount--select-unmounted-usb (emount--get-drive-list emount--usb-drive))
		     (emount--select-mount-point)))
  (let* ((partition-cmd
	  (concat "lsblk -no fstype " usb-name))
	 (partition-type
	  (string-chop-newline (shell-command-to-string partition-cmd)))
	 user-group result exit-code output)
    (if (string-equal partition-type "vfat")
	(progn
	  (setq result (ut-common-shell-command "sudo" "mount" "-t" "vfat"
						"-o" "rw,umask=0000" usb-name mount-point))
	  (setq exit-code (nth 0 result))
	  (setq output (nth 1 result))
	  (if (= exit-code emount--success-code)
	      (message "%s mounted successfully!" usb-name)
	    (user-error output)))
      (setq result (ut-common-shell-command "sudo" "mount" usb-name mount-point))
      (setq exit-code (nth 0 result))
      (setq output (nth 1 result))
      (if (not (equal exit-code emount--success-code))
	  (user-error output)
	;; TODO: test groups output
	(setq user-group (nth 0 (split-string (shell-command-to-string "groups"))))
	(setq result (ut-common-shell-command "sudo" "chowm" (user-login-name)
					      ":" user-group mount-point))
        (setq exit-code (nth 0 result))
	(setq output (nth 1 result))
	(if (= exit-code emount--success-code)
	    (message "%s mounted successfully!" usb-name)
	  (user-error output))))))

(defvar emount--android-device
  "simple-mtpfs -l 2>/dev/null"
  "Shell command to get android device names.")

(defun emount--get-android-list ()
  "Run `android-device' and store result as a list."
  (split-string
   (shell-command-to-string emount--android-device) "\n" 'omit-nill))

(defun emount-mount-android (android mount-point)
  "Mount usb with ANDROID at MOUNT-POINT."
  (interactive (list (completing-read "Mount which android device: "
				      (emount--get-android-list))
		     (emount--select-mount-point)))
  ;; android => number : name
  (let* ((android-list (split-string android ":"))
	 (android-number (nth 0 android-list))
	 (android-name (nth 1 android-list))
	 (result (ut-common-shell-command "simple-mtpfs" "--device"
					  android-number
					  mount-point))
	 (exit-code (nth 0 result))
	 (output (nth 1 result)))
    (if (= exit-code emount--success-code)
	(message "%s mounted successfully!" android-name)
      (user-error output))))

(defvar emount--list-drive
  "lsblk -nrpo 'name,type,size,mountpoint'"
  "Shell command to list drives.")

(defun emount--select-umount-usb (drive-list)
  "Select usb to umount from DRIVE-LIST.
Exclude drive mounted and /, /home, /boot and SWAP."
  (let (mount-point umount-usb)
    (dolist (drive drive-list)
      ;; drive => (name type size mount-point)
      (setq mount-point
	    (nth 3 (split-string drive " " 'omit-nill)))
      ;; remove /, /home, /boot and SWAP from umount options
      (when (and mount-point
		 (> (length mount-point) 1)
		 (not (string-match "~\\|/boot\\|/home\\|SWAP" mount-point)))
	(setq umount-usb (cons mount-point umount-usb))))
    (completing-read "Choose usb to be umounted: " umount-usb)))

(defun emount-umount-usb (mount-point)
  "Umount usb mounted at MOUNT-POINT."
  (interactive (list (emount--select-umount-usb (emount--get-drive-list emount--list-drive))))
  (let* ((result (ut-common-shell-command "sudo" "umount" mount-point))
	 (exit-code (nth 0 result))
	 (output (nth 1 result)))
    (if (equal exit-code emount--success-code)
	(message "%s umount successful" mount-point)
      (user-error output))))

(defun emount--select-umount-android ()
  "Select android to umount from /etc/mtab."
  (let ((mstab (find-file-noselect "/etc/mtab"))
	umount-android)
    (with-current-buffer mstab
      (goto-char (point-min))
      (while (re-search-forward "simple-mtpfs" (point-max) 'no-error)
	(let* ((start (line-beginning-position))
	       (end (line-end-position))
	       (line (split-string (buffer-substring start end)))
	       (android-name (nth 1 line)))
	  (setq umount-android
		(cons android-name umount-android)))
	(forward-line)))
    (kill-buffer mstab)
    (completing-read "Choose android to umounted: " umount-android)))

(defun emount-umount-android (mount-point)
  "Umount android mounted at MOUNT-POINT."
  (interactive (list (emount--select-umount-android)))
  (let* ((result (ut-common-shell-command "sudo" "umount" "-l" mount-point))
	 (exit-code (nth 0 result))
	 (output (nth 1 result)))
    (if (equal exit-code emount--success-code)
	(message "Android's umount successful")
      (user-error output))))

(provide 'emount)
;;; emount.el ends here

;;; ut-simple.el --- Common commands for my dotemacs -*- lexical-binding: t -*-

;; Copyright (c) 2020, 2021, 2022, 2023 Utkarsh Singh <utkarsh190601@gmail.com>

;; Author: Utkarsh Singh <utkarsh190601@gmail.com>
;; Package-Requires: ((emacs "27.1"))

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or (at
;; your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Common commands for my Emacs.
;; https://gitlab.com/utkarsh181/dotfiles

;;; Code:

(defgroup ut-simple ()
  "Generic utilities for my dotemacs."
  :group 'editing)

(defcustom ut-simple-date-specifier "%F"
  "Date specifier for `format-time-string'.
Used by `ut-simple-inset-date'."
  :type 'string
  :group 'ut-simple)

(defcustom ut-simple-time-specifier "%R %z"
  "Time specifier for `format-time-string'.
Used by `ut-simple-inset-date'."
  :type 'string
  :group 'ut-simple)

(defcustom ut-simple-scratch-buffer-default-mode 'lisp-interaction-mode
  "Default major mode for `ut-simple-scratch-buffer'."
  :type 'symbol
  :group 'ut-simple)

;;; Generic setup

;;;; Scratch buffers
;; The idea is based on the `scratch.el' package by Ian Eure:
;; <https://github.com/ieure/scratch-el>.

;; Adapted from the `scratch.el' package by Ian Eure.
(defun ut-simple--scratch-list-modes ()
  "List known major modes."
  (cl-loop for sym the symbols of obarray
           for name = (symbol-name sym)
           when (and (functionp sym)
                     (not (member sym minor-mode-list))
                     (string-match "-mode$" name)
                     (not (string-match "--" name)))
           collect name))

(defun ut-simple--scratch-buffer-setup (region &optional mode)
  "Add contents to `scratch' buffer and name it accordingly.

REGION is added to the contents to the new buffer.

Use the current buffer's major mode by default.  With optional
MODE use that major mode instead."
  (let* ((major (or mode major-mode))
         (string (format "Scratch buffer for: %s\n\n" major))
         (text (concat string region))
         (buf (format "*Scratch for %s*" major)))
    (with-current-buffer (get-buffer-create buf)
      (funcall major)
	  (save-excursion
        (insert text)
        (goto-char (point-min))
        (comment-region (point-at-bol) (point-at-eol)))
	  (vertical-motion 2))
    (pop-to-buffer buf)))

(defun ut-simple-scratch-buffer (&optional arg)
  "Produce a bespoke scratch buffer matching current major mode.

With optional ARG as a prefix argument (\\[universal-argument]),
use `ut-simple-scratch-buffer-default-mode'.

With ARG as a double prefix argument, prompt for a major mode
with completion.

If region is active, copy its contents to the new scratch
buffer."
  (interactive "P")
  (let* ((default-mode ut-simple-scratch-buffer-default-mode)
         (modes (ut-simple--scratch-list-modes))
         (region (with-current-buffer (current-buffer)
                   (if (region-active-p)
                       (buffer-substring-no-properties
                        (region-beginning)
                        (region-end))
                     "")))
         (m))
    (pcase (prefix-numeric-value arg)
      (16 (progn
            (setq m (intern (completing-read "Select major mode: " modes nil t)))
            (ut-simple--scratch-buffer-setup region m)))
      (4 (ut-simple--scratch-buffer-setup region default-mode))
      (_ (ut-simple--scratch-buffer-setup region)))))

(defun ut-simple-new-line-below (&optional arg)
  "Create an empty line below the current one.
Move the point to the indented area.  Adapt indentation by
passing \\[universal-argument].  Also see `new-line-above'."
  (interactive "P")
  (end-of-line)
  (if arg
      (newline-and-indent)
    (newline))
  (indent-according-to-mode))

(defun ut-simple-new-line-above (&optional arg)
  "Create an empty line above the current one.
Move the point to the absolute beginning.  Adapt indentation by
passing \\[universal-argument]."
  (interactive "P")
  (let ((indent (if arg arg nil)))
    (if (or (bobp)
            (line-number-at-pos 1))
        (progn
          (beginning-of-line)
          (newline)
          (forward-line -1))
      (forward-line -1)
      (new-line-below indent))))

(defun ut-simple-multi-line-next ()
  "Move point 15 lines down."
  (interactive)
  (forward-line 15))

(defun ut-simple-multi-line-prev ()
  "Move point 15 lines up."
  (interactive)
  (forward-line -15))

(defun ut-simple-kill-line-backward ()
  "Kill from point to the beginning of the line."
  (interactive)
  (kill-line 0))

(defun ut-simple-transpose-words (arg)
    "Transpose ARG words.

If region is active, swap the word at mark (region beginning)
with the one at point (region end).

Otherwise, and while inside a sentence, this behaves as the
built-in `transpose-words', dragging forward the word behind the
point.  The difference lies in its behaviour at the end or
beginnning of a line, where it will always transpose the word at
point with the one behind or ahead of it (effectively the
last/first two words)."
  (interactive "*p")
  (cond
   ((use-region-p) (transpose-words 0))
   ((bolp) (transpose-words 1))
   ((eolp) (transpose-words -1))
   (t (transpose-words arg))))

(defun ut-simple-transpose-lines (arg)
  "Transpose ARG lines or swap over active region."
  (interactive "*p")
  (cond
   ((use-region-p) (transpose-lines 0))
   (t (transpose-lines arg))))

(defun ut-simple-headerise-elisp ()
  "Add minimal header and footer to an elisp buffer in order to placate flycheck."
  (interactive)
  (let ((fname (if (buffer-file-name)
                   (file-name-nondirectory (buffer-file-name))
                 (error "This buffer is not visiting a file"))))
    (save-excursion
      (goto-char (point-min))
      (insert ";;; " fname " --- Insert description here -*- lexical-binding: t -*-\n"
              ";;; Commentary:\n"
              ";;; Code:\n\n")
      (goto-char (point-max))
      (insert ";;; " fname " ends here\n"))))

(defun ut-simple-insert-date (&optional arg)
  "Insert the current date as `ut-simple-date-specifier'.

With optional prefix ARG (\\[universal-argument]) also append the
current time understood as `ut-simple-time-specifier'.

When region is active, delete the highlighted text and replace it
with the specified date."
  (interactive "P")
  (let* ((date ut-simple-date-specifier)
         (time ut-simple-time-specifier)
         (format (if arg (format "%s %s" date time) date)))
    (when (use-region-p)
      (delete-region (region-beginning) (region-end)))
    (insert (format-time-string format))))

(defun ut-simple-rename-file-and-buffer ()
  "Rename current buffer and if the buffer is visiting a file, rename it too."
  (interactive)
  (let ((filename (buffer-file-name)))
    (if (not (and filename (file-exists-p filename)))
        (rename-buffer (read-from-minibuffer "New name: " (buffer-name)))
      (let* ((new-name (read-file-name "New name: " (file-name-directory filename)))
             (containing-dir (file-name-directory new-name)))
        (make-directory containing-dir t)
        (cond
         ((vc-backend filename) (vc-rename-file filename new-name))
         (t
          (rename-file filename new-name t)
          (set-visited-file-name new-name t t)))))))

(defalias 'ut-simple-rename-buffer-and-file #'ut-simple-rename-file-and-buffer)

(provide 'ut-simple)
;;; ut-simple.el ends here

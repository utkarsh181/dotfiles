(use-package message
  :config
  (defun ut/message-header-add-gcc ()
    "While `gnus' is running, add pre-populated Gcc header.

The Gcc header places a copy of the outgoing message to the
appropriate directory of the IMAP server, as per the contents of
~/.authinfo.gpg.

In the absence of a Gcc header, the outgoing message will not
appear in the appropriate maildir directory, though it will still
be sent.

Add this function to `message-header-setup-hook'."
    (if (gnus-alive-p)
        (progn
          (when (message-fetch-field "Gcc")
            (message-remove-header "Gcc"))
          (message-add-header "Gcc: nnmaildir+utkarsh190601@gmail.com:[Gmail].Sent"))
      (message "Gnus is not running. No GCC field inserted.")))

  (add-hook 'message-header-setup-hook #'ut/message-header-add-gcc)
  (add-hook 'message-setup-hook #'message-sort-headers))

;; news reader
(use-package gnus
  :init
  (setq gnus-home-directory
	(ut-common-concat-filename user-emacs-directory "gnus/"))
  :config
  (setq gnus-select-method '(nnnil ""))
  (setq gnus-secondary-select-methods
        '((nntp "news.gwene.org")
	  (nnmaildir "utkarsh190601@gmail.com"
		     (directory (ut-common-concat-filename message-directory
							   "utkarsh190601@gmail.com/")))))
  (setq gnus-gcc-mark-as-read t)
  (setq gnus-asynchronous t))

(use-package gnus-group
  :config
  (setq gnus-group-line-format "%M%p%P%5y:%B%(%g%)\n")
  (setq gnus-group-mode-line-format "%%b")
  (add-hook 'gnus-group-mode-hook #'gnus-topic-mode)
  (add-hook 'gnus-group-mode-hook #'hl-line-mode))

(use-package gnus-sum
  :config
  (setq gnus-auto-select-first nil)
  (setq gnus-thread-sort-functions
        '((not gnus-thread-sort-by-date)
          (not gnus-thread-sort-by-number)))
  (setq gnus-subthread-sort-functions
        'gnus-thread-sort-by-date)
  (setq gnus-user-date-format-alist
        '(((gnus-seconds-today) . "Today at %R")
          ((+ (* 60 60 24) (gnus-seconds-today)) . "Yesterday, %R")
          (t . "%Y-%m-%d %R")))
  (setq gnus-summary-line-format "%U%R %-18,18&user-date; %4L:%-25,25f %B%s\n")
  (setq gnus-summary-mode-line-format "[%U] %p")
  (add-hook 'gnus-summary-mode-hook #'hl-line-mode)
  (let ((map gnus-summary-mode-map))
    (define-key map (kbd "n") #'gnus-summary-next-article)
    (define-key map (kbd "p") #'gnus-summary-prev-article)
    (define-key map (kbd "N") #'gnus-summary-next-unread-article)
    (define-key map (kbd "P") #'gnus-summary-prev-unread-article)
    (define-key map (kbd "M-n") #'gnus-summary-next-thread)
    (define-key map (kbd "M-p") #'gnus-summary-prev-thread)
    (define-key map (kbd "C-M-n") #'gnus-summary-next-group)
    (define-key map (kbd "C-M-p") #'gnus-summary-prev-group)
    (define-key map (kbd "C-M-^") #'gnus-summary-refer-thread)))

(use-package gnus-art
  :config
  (setq gnus-article-mode-line-format "Gnus: %p %S%m")
  (setq smiley-style 'emoji))

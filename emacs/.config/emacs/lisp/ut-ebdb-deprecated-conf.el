(use-package ebdb
  :ensure t
  :config
  (require 'ebdb-message)
  (require 'ebdb-notmuch)

  (setq ebdb-sources (locate-user-emacs-file "ebdb"))
  (setq ebdb-permanent-ignores-file (locate-user-emacs-file "ebdb-permanent-ignores"))

  (setq ebdb-mua-auto-update-p 'existing)
  (setq ebdb-mua-reader-update-p 'existing)
  (setq ebdb-mua-sender-update-p 'create)
  (setq ebdb-message-auto-update-p 'create)

  (setq ebdb-message-try-all-headers t)
  (setq ebdb-message-headers
        '((sender "From" "Resent-From" "Reply-To" "Sender")
          (recipients "Resent-To" "Resent-Cc" "Resent-CC" "To" "Cc" "CC" "Bcc" "BCC")))
  (setq ebdb-message-all-addresses t)

  (setq ebdb-complete-mail 'capf)
  (setq ebdb-completion-display-record nil)

  (setq ebdb-save-on-exit t)

  (defun ut/ebdb-message-setup ()
    "Load EBDB if not done already.
Meant to be assigned to a hook, such as `message-setup-hook'."
    (unless ebdb-db-list
      (ebdb-load)))

  (add-hook 'message-setup-hook #'ut/ebdb-message-setup))

;;; ut-outline.el --- Extend outline.el for my dotemacs -*- lexical-binding: t -*-

;; Copyright (C) 2021, 2022, 2023 Utkarsh Singh

;; Author: Utkarsh Singh <utkarsh190601@gmail.com>
;; URL: https://gitlab.com/utkarsh181/dotfiles
;; Package-Requires: ((emacs "28.1"))

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or (at
;; your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Extensions to the built-in `outline.el' library for my Emacs
;; configuration:
;; https://gitlab.com/utkarsh181/dotfiles

;;; Code:

;;;; Commands

(defun ut-outline-narrow-to-subtree ()
  "Narrow buffer to current subtree."
  (interactive)
  (save-excursion
    (narrow-to-region
     (progn (outline-back-to-heading t) (point))
     (progn (outline-end-of-subtree) (point)))))

;;;; Minor mode setup

(defvar ut-outline-major-modes-blocklist '(org-mode outline-mode markdown-mode)
  "List of major mode that does not play safe with `outline-mimor mode'.")

(defun ut-outline-minor-mode-safe ()
  "Test to set variable `outline-minor-mode' to non-nil."
  (interactive)
  (let ((blocklist ut-outline-major-modes-blocklist)
        (mode major-mode))
    (when (derived-mode-p (car (member mode blocklist)))
      (error "Don't use `ut-outline-minor-mode' with `%s'" mode))
    (if (eq outline-minor-mode nil)
        (progn
          (outline-minor-mode 1)
          (message "Enabled `outline-minor-mode'"))
      (outline-minor-mode -1)
      (message "Disabled `outline-minor-mode'"))))

(provide 'ut-outline)
;;; ut-outline.el ends here

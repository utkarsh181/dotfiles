
;;;; PDF reader

(use-package pdf-occur
  :commands pdf-occur-global-minor-mode)

(use-package pdf-history
  :commands pdf-history-minor-mode)

(use-package pdf-links
  :commands pdf-links-minor-mode)

(use-package pdf-outline
  :commands pdf-outline-minor-mode)

(use-package pdf-annot
  :commands pdf-annot-minor-mode)

(use-package pdf-sync
  :commands pdf-sync-minor-mode)

(use-package pdf-tools
  :init
  (pdf-tools-install :no-query)
  :config
  (defun ut/pdf-tools-backdrop ()
    (modus-themes-with-colors
      (face-remap-add-relative
       'default
       `(:background ,bg-dim))))

  (defun ut/pdf-tools-midnight-mode-toggle ()
    (if (eq (car custom-enabled-themes) 'modus-vivendi)
        (pdf-view-midnight-minor-mode 1)
      (pdf-view-midnight-minor-mode -1))
    (ut/pdf-tools-backdrop))

  (defun ut/pdf-tools-themes-toggle ()
    (mapc
     (lambda (buf)
       (with-current-buffer buf
	 (when (derived-mode-p 'pdf-view-mode)
	   ;; Alternatively use `ut/pdf-tools-midnight-mode-toggle'
	   (ut/pdf-tools-backdrop))))
     (buffer-list)))

  (defun ut/pdf-tools-ut-modelines-position ()
    (setq-local ut-modeline-position
		'(" P" (:eval (number-to-string (pdf-view-current-page)))
                  ;; Avoid errors during redisplay.
                  "/" (:eval (or (ignore-errors
                                   (number-to-string (pdf-cache-number-of-pages)))
				 "???")))))
  :magic ("%PDF" . pdf-view-mode)
  :commands (pdf-tools-install)
  :hook ((pdf-view-mode . ut/pdf-tools-backdrop)
	 (pdf-view-mode . ut/pdf-tools-ut-modelines-position)
	 (modus-themes-after-load-theme . ut/pdf-tools-themes-toggle))
  :bind ( :map pdf-view-mode-map
	  ("C-s" . isearch-forward)
	  ("<left>" . pdf-view-previous-page-command)
	  ("<right>" . pdf-view-next-page-command))
  :custom
  (pdf-view-display-size 'fit-page)
  (pdf-view-resize-factor 1.125))

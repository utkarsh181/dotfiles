;;; ut-fill.el --- Minor fill-mode tweaks for my dotemacs -*- lexical-binding: t -*-

;; Copyright (C) 2021, 2022, 2023 Utkarsh Singh

;; Author: Utkarsh Singh <utkarsh190601@gmail.com>
;; Package-Requires: ((emacs "27.1"))

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; This covers my fill-mode extensions, for use in my Emacs setup:
;; https://gitlab.com/utkarsh181/dotfiles

;;; Code:

(defgroup ut-fill ()
  "Tweak for filling paragraphs."
  :group 'fill)

(defcustom ut-fill-prog-mode-column 80
  "`prog-mode' width for `fill-column'.
Also see `ut-fill-default-column'."
  :type 'integer
  :group 'ut-fill)

(defun ut-fill--fill-prog ()
  "Set local value of `fill-column' for programming modes.
Meant to be called via `prog-mode-hook'."
  (setq-local fill-column ut-fill-prog-mode-column))

;;;###autoload
(define-minor-mode ut-fill-fill-mode
  "Set up fill-mode and relevant variable."
  :init-value nil
  :global t
  (if ut-fill-fill-mode
      (progn
        (add-hook 'prog-mode-hook #'ut-fill--fill-prog)
        (add-hook 'text-mode-hook #'turn-on-auto-fill))
    (remove-hook 'prog-mode-hook #'ut-fill--fill-prog)
    (remove-hook 'text-mode-hook #'turn-on-auto-fill)))

(provide 'ut-fill)
;;; ut-fill.el ends here

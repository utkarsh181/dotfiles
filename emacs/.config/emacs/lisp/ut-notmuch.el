;;; ut-notmuch.el --- Extension to notmuch.el        -*- lexical-binding: t; -*-

;; Copyright (C) 2021, 2022, 2023 Utkarsh Singh

;; Author: Utkarsh Singh <utkarsh190601@gmail.com>
;; Keywords: convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This covers my extension for notmuch.el that are meant for use in my Emacs
;; setup: <https://gitlab.com/utkarsh181/dotfiles>.

;;; Code:

(when (featurep 'notmuch)
  (require 'notmuch))

(defgroup ut-notmuch ()
  "Extensions for notmuch.el."
  :group 'notmuch)

(defcustom ut-notmuch-delete-tag "deleted"
  "Single tag that applies to mail marked for deletion.
This is used by `ut-notmuch-delete-mail'."
  :type 'string
  :group 'ut-notmuch)

(defun ut-notmuch--run (&rest args)
  "Run notmuch with ARGS.

Nil arguments are ignored.  Returns the output on success, or
outputs error message on failure."
  (with-temp-buffer
    (notmuch-call-notmuch--helper t (delq nil args))
    (buffer-string)))

;;; Commands

(defun ut-notmuch-delete-mail ()
  "Permanently delete mail marked as `ut-notmuch-delete-mail'.
Prompt for confirmation before carrying out the operation.

Do not attempt to refresh the index.  This will be done upon the
next invocation of 'notmuch new'."
  (interactive)
  (let* ((del-tag (concat "tag:" ut-notmuch-delete-tag))
         (count (string-to-number (ut-notmuch--run "count" del-tag)))
         (mail (if (> count 1) "mails" "mail"))
	 files)
    (unless (> count 0)
      (user-error "No mail marked as `%s'" del-tag))
    (when (yes-or-no-p
           (format "Delete %d %s marked as `%s'?" count mail del-tag))
      (dolist (file (split-string
		     (ut-notmuch--run "search""--output=files" del-tag)))
	(when (file-regular-p file)
	  (delete-file file))))))

(provide 'ut-notmuch)
;;; ut-notmuch.el ends here

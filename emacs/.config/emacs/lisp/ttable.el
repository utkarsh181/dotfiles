;;; ttable.el --- Time table editing utilities for Org Mode -*- lexical-binding:t -*-

;; Copyright (C) 2021, 2022, 2023 Utkarsh Singh

;; Author: Utkarsh Singh <utkarsh190601@gmail.com>
;; Package-Requires: ((emacs "27.1") (pcsv))
;; Keywords: org-mode, orgtbl

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or (at
;; your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; This files is used to create and edit and time table presented as Org mode
;; tables.
;;
;; Checkout out my blog entry to know more about how to make best use of it:
;; https://www.utkarshsingh.xyz/blog/time-table-with-emacs.html.

;;; Code:

(require 'org-table)
(require 'subr-x)

(when (featurep 'pcsv)
  (require 'pcsv))

(declare-function pcsv-parse-file "pcsv" (file &optional coding-system))

(defgroup ttable nil
  "Time table editing utilities for Org mode."
  :tag "Time Table"
  :group 'org)

(defcustom ttable-buffer-name "*ttable-output*"
  "Name of buffer holding `ttable-create'."
  :type 'string)

(defvar ttable--class-days
  '("mon" "tue" "wed" "thu" "fri" "sat" "sun")
  "List of working days of a week.")

(defvar ttable-header '("Days" "9:00 - 10:00" "10:00 - 11:00"
			"11:00 - 12:00" "12:00 - 13:00" "13:00 - 14:00"
			"14:00 - 15:00" "15:00 - 16:00" "16:00 - 17:00"
			"17:00 - 18:00")
  "Header for time table.")

(defun ttable--get-ttable-buffer ()
  "Prepare `ttable-buffer-name' buffer."
  (let ((buf (get-buffer ttable-buffer-name))
	(inhibit-read-only t))
    (with-current-buffer buf
      (erase-buffer))))

(defun ttable--insert-header ()
  "Insert header to the time table."
  (goto-char (point-min))
  (insert "#+TITLE: Time table\n")
  (insert (format "#+AUTHOR: %s\n" user-full-name))
  (insert (format "#+EMAIL: %s\n\n" user-mail-address))
  (dolist (i ttable-header)
    (insert "| " i))
  (insert " |" "\n")
  (insert "|-\n")
  (save-excursion
    (goto-char (point-max))
    (insert "\n# Local Variables:\n"
	    "# eval: (hl-line-mode 1)\n"
	    "# End:\n")))

(defun ttable-create (file class)
  "Create a time table from FILE for CLASS.

Format of a time table:

1. Every line should contain equal number of column.
2. Weekdays should be separated by an empty row (denoted by DAY-SEP).
3. Weekdays should be in same sequence as `ttable--class-days'.

Representation of a CSV file containing a time table:

day 1, class 1, class 2, ... class m-3
,class m-2, class m-1, ... class m,
,,, ... m + 1 times
...
...
...
day n, ..."
  (interactive (list (read-file-name "Find file: " nil nil t)
		     (read-regexp "Enter class: ")))
  (let* ((csv (pcsv-parse-file file))
	 (days ttable--class-days)
	 (class-frequency (length (car csv)))
	 (day-sep (make-list class-frequency ""))
	 (vec (make-vector (1- class-frequency) ""))
	 (index 0)
	 list)
    (pop-to-buffer ttable-buffer-name)
    (ttable--get-ttable-buffer)
    (ttable--insert-header)
    ;; parse CSV
    (dolist (i csv)
      (if (and (equal i day-sep) days)
	  ;; add vector to list
	  (progn
	    (push vec list)
	    (setq days (cdr days))
	    (setq vec (make-vector (1- class-frequency) "")))
	;; create a vector for a class in a day
	(setq index 0)
	(dolist (j i)
	  (when (string-match-p class j)
	    (setf (seq-elt vec (1- index)) j))
	  (setq index (1+ index)))))
    (setq list (reverse list))
    (setq days ttable--class-days)
    ;; print table
    (dolist (i list)
      (insert "| " (car days))
      (setq days (cdr days))
      (cl-map 'vector (lambda (x) (insert "| " x)) i)
      (insert " |" "\n"))
    (org-table-align)))

(provide 'ttable)
;;; ttable ends here

;;; ut-common.el --- Common function for my dotemacs -*- lexical-binding:t -*-

;; Copyright (C) 2021, 2022, 2023 Utkarsh Singh

;; Author: Utkarsh Singh <utkarsh190601@gmail.com>
;; Package-Requires: ((emacs "27.1"))

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or (at
;; your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Common functions for my Emacs:
;; https://gitlab.com/utkarsh181/dotfiles

;;; Code:

(defgroup ut-common()
  "Auxiliary functions for my dotemacs."
  :group 'editing)

(defun ut-common-shell-command (command &rest args)
  "Run COMMAND with ARGS.
Return the exit code and output in a list."
  (with-temp-buffer
    (list (apply 'call-process command nil (current-buffer) nil args)
          (buffer-string))))

(defun ut-common-concat-filename (dir &rest files)
  "Concat DIR as directory to FILES."
  (apply #'concat (file-name-as-directory dir) files))

(defvar ut-common--fuzz-factor 1.0e-6
  "Fuzz factor used in `ut-common-approx-equal' to compare
floating-points value.")

(defun ut-common-approx-equal (num1 num2)
  "Compare NUM1 and NUM2 approximately.

As floating-point arithmetic is not exact, it is often a bad idea
to check for equality of floating-point values.  Usually it is
better to test for approximate equality."
  (or (= num1 num2)
      (< (/ (abs (- num1 num2))
            (max (abs num1) (abs num2)))
         ut-common--fuzz-factor)))

(defun ut-common-decimal-to-binary (num)
  "Convert decimal NUM to binary number.
Format and return binary number to string by using 2# and -2# as
prefix for positive and negative NUM respectively.

If NUM in negative then calculate it's 2's complement.  Construct
string by iterating over NUM bit's by right arithmetic shift and
then obtaining bit's value by bitwise AND of NUM and 1."
  (interactive (list (read-number "Enter number: ")))
  (let (str isneg rel)
    (unless (integerp num)
      (user-error "%s is not an integer" num))
    (when (< num 0)
      (setq num (1+ (lognot num)))
      (setq isneg t))
    (while (> num 0)
      (setq str (concat str (number-to-string (logand num 1))))
      (setq num (ash num -1)))
    (setq rel
	  (if isneg
	      (concat "-2#" (reverse str))
	    (concat "2#" (reverse str))))
    (if (called-interactively-p 'any)
	(message "%s" rel)
      rel)))

(provide 'ut-common)
;;; ut-common.el ends here

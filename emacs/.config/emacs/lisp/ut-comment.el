;;; ut-comment.el --- Extension to newcomment.el     -*- lexical-binding: t; -*-

;; Copyright (C) 2021, 2022, 2023 Utkarsh Singh

;; Author: Utkarsh Singh <utkarsh190601@gmail.com>
;; Keywords: convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Extensions for newcomment.el for my Emacs setup:
;; <https://gitlab.com/utkarsh181/dotfiles>

;;; Code:

(require 'newcomment)

(defgroup ut-comment ()
  "Extensions for newcomment.el."
  :group 'comment)

(defcustom ut-comment-keywords
  '("TODO" "NOTE" "XXX" "REVIEW" "FIXME")
  "List of strings with comment keywords."
  :type '(repeat string)
  :group 'ut-comment)

(defvar ut-comment--keyword-hist '()
  "Input history of selected comment keywords.")

(defcustom ut-comment-timestamp-format-concise "%F"
  "Specifier for date in `ut-comment-timestamp-keyword'.
Refer to the doc string of `format-time-string' for the available
options."
  :type 'string
  :group 'ut-comment)

(defcustom ut-comment-timestamp-format-verbose "%F %T %z"
  "Like `ut-comment-timestamp-format-concise', but longer."
  :type 'string
  :group 'ut-comment)

(defun ut-comment--keyword-prompt (keywords)
  "Prompt for candidate among KEYWORDS."
  (let ((def (car ut-comment--keyword-hist)))
    (completing-read
     (format "Select keyword [%s]: " def)
     keywords nil nil nil 'ut-comment--keyword-hist def)))

;;;###autoload
(defun ut-comment-timestamp-keyword (&optional verbose)
  "Add timestamp keywords to comments.

Call `comment-dwim' and then insert timestamp keyword IF region
is NOT active.

With optional VERBOSE argument (such as a prefix argument
`\\[universal-argument]'), use an alternative date format, as
specified by `ut-comment-timestamp-format-verbose'."
  (interactive (list current-prefix-arg))
  (let ((date-format (if verbose
			 ut-comment-timestamp-format-verbose
		       ut-comment-timestamp-format-concise)))

    ;; REVIEW 2021-07-26: Can we make use of `comment-kill'?
    (comment-dwim nil)

    (unless (use-region-p)
      (insert (format  "%s %s: "
		       (ut-comment--keyword-prompt ut-comment-keywords)
		       (format-time-string date-format))))))

(provide 'ut-comment)
;;; ut-comment.el ends here

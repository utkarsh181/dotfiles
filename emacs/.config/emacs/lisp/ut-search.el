;;; ut-search.el --- Extension to searching utilities  -*- lexical-binding: t; -*-

;; Copyright (C) 2021, 2022, 2023 Utkarsh Singh

;; Author: Utkarsh Singh <utkarsh190601@gmail.com>
;; Keywords: convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This covers my searching utilities extensions for the use in my Emacs setup:
;; <https://gitlab.com/utkarsh181/dotfiles>

;;; Code:

(require 'browse-url)

(defgroup ut-search ()
  "Extensions for searching utilities."
  :group 'convenience)

(defun ut-search-occur-urls ()
  "Produce buttonised list of all URLs in the current buffer."
  (interactive)
  (add-hook 'occur-hook #'goto-address-mode)
  (occur browse-url-button-regexp "\\&")
  (remove-hook 'occur-hook #'goto-address-mode))

;;;###autoload
(defun ut-search-occur-browse-url (&optional arg)
  "Point browser at a URL in the buffer using completion.
Browser to used depends on `browse-url-browser-function'.

Also see `ut-search-occur-urls'."
  (interactive "P")
  (let ((browser (if (not arg)
		     browse-url-browser-function
		   browse-url-secondary-browser-function))
	(matches))
    (save-excursion
      (goto-char (point-min))
      (while (re-search-forward browse-url-button-regexp nil t)
        (push (match-string 0) matches)))
    (funcall browser
             (completing-read "Browse URL: " matches nil t))))

(provide 'ut-search)
;;; ut-search.el ends here

;;; init.el --- Personal Configuration               -*- lexical-binding: t; -*-

;; Copyright (C) 2020, 2021, 2022, 2023 Utkarsh Singh

;; Author: Utkarsh Singh <utkarsh190601@gmail.com>

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Code:

;;; Base settings

;;;; Packaging

(require 'package)

(setq package-archives
      '(("elpa" . "https://elpa.gnu.org/packages/")
        ("nongnu" . "https://elpa.nongnu.org/nongnu/")
        ("melpa" . "https://melpa.org/packages/")))

(setq package-archive-priorities
      '(("elpa" . 2)
        ("nongnu" . 1)))

(add-hook 'package-menu-mode-hook #'hl-line-mode)

;; configure `use-package' prior to loading it.
(eval-and-compile
  (setq use-package-always-ensure nil))

(eval-when-compile
  (require 'use-package))
(require 'bind-key)

(dolist (path '("lisp"))
  (add-to-list 'load-path (thread-last user-emacs-directory (expand-file-name path))))

(use-package emacs
  :bind (("C-z" . nil))
  :custom
  (use-short-answers t)
  (custom-file (make-temp-file "emacs-custom-"))
  (find-function-C-source-directory "~/.local/src/emacs-git/src/emacs-git/src")
  :config
  (put 'narrow-to-region 'disabled nil)
  (put 'overwrite-mode 'disabled nil)
  (put 'dired-find-alternate-file 'disabled nil)
  (put 'downcase-region 'disabled nil)
  (put 'upcase-region 'disabled nil))

;;;; Common auxiliary function

(use-package ut-common)

;;;; Common custom function

(use-package ut-simple
  :bind (("M-SPC" . cycle-spacing)	; bounded to windows-menu in gnome-shell
	 ("M-o" . delete-blank-lines)	; alias for C-x C-o
	 ("M-=" . count-words)
	 ("M-t" . ut-simple-transpose-words)
	 ("M-k" . ut-simple-kill-line-backward)
	 ("C-S-n" . ut-simple-multi-line-next)
	 ("C-S-p" . ut-simple-multi-line-prev)
	 ("C-<return>" . ut-simple-new-line-below)
	 ("C-S-<return>" . ut-simple-new-line-above)
	 ("C-c s" . ut-simple-scratch-buffer))
  :commands
  ut-simple-insert-date)

;;;; Theme and typeface configuration

(use-package emacs
  :bind ("<f5>" . toggle-theme)
  :config
  ;; Load a light/dark theme based on some general time ranges (just accounting
  ;; for the hour and without checking for the actual sunrise/sunset times).
  (let ((time (string-to-number (format-time-string "%H"))))
    (if (and (> time 5) (< time 18))
        (load-theme 'modus-operandi)
      (load-theme 'modus-vivendi))))

(use-package emacs
  :config
  (add-to-list 'default-frame-alist '(font . "Monospace-11")))

;;; Selection candidate and search method

;;;; Completion framework

(use-package minibuffer
  :custom
  (echo-keystrokes 0.25)
  (completions-format 'one-column)
  (enable-recursive-minibuffers t))

;; completion style
(use-package orderless
  :ensure t
  :custom
  (completion-styles '(orderless basic))
  (completion-category-defaults nil)
  (completion-category-overrides '((file (styles . (partial-completion))))))

(use-package vertico
  :ensure t
  :config
  (vertico-mode 1))

(use-package corfu
  :ensure t
  :custom
  (corfu-quit-at-boundary nil)
  :init
  (global-corfu-mode 1)
  (corfu-popupinfo-mode 1))

;;;; Search command

(use-package isearch
  :bind (:map isearch-mode-map ("C-g" . isearch-cancel))
  :custom
  (search-whitespace-regexp ".*?")
  (isearch-lax-whitespace t)
  (isearch-regexp-lax-whitespace nil)
  (isearch-lazy-count t)
  (lazy-count-prefix-format nil)
  (lazy-count-suffix-format " (%s/%s)"))

;; Extensions to searching utilities
(use-package ut-search)

;;;; Projects

(use-package project
  :custom
  (project-switch-commands
   '((?f "File" project-find-file)
     (?g "Grep" project-find-regexp)
     (?d "Dired" project-dired)
     (?v "Vterm" project-vterm)
     (?m "Magit" magit-project-status)))
  :config
  (defun ut/project-try-explicit (dir)
    "Find a super-directory of DIR containing a root file."
    (locate-dominating-file dir ".root"))

  (cl-defmethod project-root ((project string))
    "Return the root of a explicit PROJECT."
    project)

  (cl-defmethod project-ignores ((project string) dir)
    "Check .root file for files to ignore in PROJECT's DIR."
    (let ((ignore (expand-file-name ".ignore" dir)))
      (when (file-exists-p ignore)
	(with-temp-buffer
          (insert-file-contents ignore)
          (nconc (cons ".ignore" (split-string (buffer-string) "\n" t))
		 (cl-call-next-method project dir))))))

  (defun project-vterm (&optional arg)
  "Start Vterm in the current project's root directory.
If a buffer already exists for running Vterm in the project's root,
switch to it.  Otherwise, create a new Vterm buffer.
With \\[universal-argument] prefix arg, create a new Vterm buffer even
if one already exists."
  (interactive "P")
  (defvar vterm-buffer-name)
  (let* ((default-directory (project-root (project-current t)))
         (vterm-buffer-name (project-prefixed-buffer-name "vterm"))
         (vterm-buffer (get-buffer vterm-buffer-name)))
    (if (and vterm-buffer (not current-prefix-arg))
        (pop-to-buffer vterm-buffer (bound-and-true-p display-comint-buffer-action))
      (vterm arg))))

  ;; TODO 2022-12-01: Can we use :hook here?
  (add-hook 'project-find-functions #'ut/project-try-explicit)

  (add-to-list 'project-kill-buffer-conditions '(major-mode . vterm-mode)))

;;; Directory, buffer and window management

;;;; Dired (file manager)

(use-package dired
  :bind (("C-x C-d" . nil))
  :hook ((dired-mode . dired-hide-details-mode)
	 (dired-mode . hl-line-mode))
  :custom
  (dired-recursive-copies 'always)
  (dired-recursive-deletes 'always)
  (delete-by-moving-to-trash t)
  (dired-listing-switches
   "-AGFhlv --group-directories-first --time-style=long-iso")
  (dired-free-space nil))

;; Dired extensions
(use-package ut-dired
  :config
  (setq dired-guess-shell-alist-user
	`((,ut-dired-image-extensions (ut-dired-image-viewer))
	  (,ut-dired-media-extensions (ut-dired-media-player))
	  (,ut-dired-office-extensions (ut-dired-office-suite))
	  (,ut-dired-pdf-extensions (ut-dired-pdf-viewer)))))

(use-package dired-aux
  :custom
  (dired-create-destination-dirs 'ask)
  (dired-vc-rename-file t))

(use-package image-dired
  :custom
  (image-dired-external-viewer "eog"))

;;;; Dired-like mode for Trash

(use-package trashed
  :ensure t
  :custom
  (trashed-date-format "%Y-%m-%d %H:%M"))

;;;; Working with buffers

(use-package emacs
  :bind (("C-c d" . dired-other-window)
	 ("C-c f" . find-file-other-window)
	 ("C-c b" . switch-to-buffer-other-window)))

;; manage buffer with identically-named files
(use-package uniquify
  :custom
  (uniquify-buffer-name-style 'post-forward-angle-brackets)
  (uniquify-strip-common-suffix t)
  (uniquify-after-kill-buffer-p t))

;; reverts buffer is file is change on disk
(use-package autorevert
  :custom
  (auto-revert-verbose t)
  :config
  (global-auto-revert-mode 1))

(use-package ibuffer
  :bind (("C-x C-b" . ibuffer)))

;;;; Window config

(use-package window
  :custom
  (display-buffer-alist
   '(("\\*\\(.*e?shell\\|tex-shell\\|ielm\\|.*vterm\\)\\*"
      (display-buffer-reuse-mode-window display-buffer-at-bottom)
      (window-height . 0.25)))))

;; undo system for window management
(use-package winner
  :init
  (setq winner-dont-bind-my-keys t)
  :config
  (winner-mode 1))

;; use ctrl + super + vim-keys to move around windows
(use-package windmove
  :bind (("C-s-k" . windmove-up)
	 ("C-s-l" . windmove-right)
	 ("C-s-j" . windmove-down)
	 ("C-s-h" . windmove-left)
	 ;; Numpad keys clockwise: 8 6 2 4
	 ("<kp-up>" . windmove-up)
	 ("<kp-right>" . windmove-right)
	 ("<kp-down>" . windmove-down)
	 ("<kp-left>" . windmove-left))
  :custom
  (windmove-create-window nil))

;;;; Frame config

(use-package frame
  :bind (("C-x C-z" . nil)
	 ("C-z" . nil)))

;;; General interface and interaction

;;;; Current time

(use-package time
  :custom
  (display-time-24hr-format t)
  (display-time-default-load-average nil))

;;;; Delete selection

;; deletes text under selection when insertion is made
(use-package delsel
  :config
  (delete-selection-mode 1))

;;;; Mode line

(use-package emacs
  :config
  (column-number-mode 1))

(use-package ut-modeline
  :custom
  (mode-line-compact nil)
  :config
  (setq-default mode-line-format
                '("%e"
                  ut-modeline-kbd-macro
                  ut-modeline-narrow
                  ut-modeline-input-method
                  ut-modeline-buffer-status
                  " "
                  ut-modeline-buffer-identification
                  "  "
                  ut-modeline-major-mode
                  ut-modeline-process
		  "  "
		  ut-modeline-position
                  "  "
                  ut-modeline-vc-branch
                  "  "
                  ut-modeline-flymake
                  "  "
                  ut-modeline-align-right
                  ut-modeline-misc-info)))

;;;; Mount USB and android

(use-package emount)

;;;; Outline mode

(use-package outline
  :config
  (setq-default outline-minor-mode-highlight 'override)
  (setq-default outline-minor-mode-cycle t))

(use-package ut-outline
  :bind(("<f10>" . ut-outline-minor-mode-safe)
	:map outline-minor-mode-map
	("C-x n s" . ut-outline-narrow-to-subtree)))

;;;; Minor extras

(use-package simple
  :custom
  (save-interprogram-paste-before-kill t))

;;;; Mouse

(use-package mouse
  :init
  (setq context-menu-functions
	'(context-menu-ffap
          occur-context-menu
          context-menu-region
          context-menu-undo
          dictionary-context-menu))
  :config
  (context-menu-mode 1))

;;;; Tab Bar

(use-package tab-bar
  :bind (:map tab-bar-history-mode-map
	      ("C-c <left>" . nil)
	      ("C-c <right>" . nil))
  :custom
  (tab-bar-show 1)
  (tab-bar-tab-name-function #'tab-bar-tab-name-truncated)
  (tab-bar-close-button-show nil)
  (tab-bar-format '(tab-bar-format-tabs tab-bar-separator))
  :config
  (tab-bar-mode 1)
  (tab-bar-history-mode 1))

(use-package ut-tab
  :bind (("C-x <right>" . ut-tab-winner-redo)
	 ("C-x <left>" . ut-tab-winner-undo)
	 ("C-x t t" . ut-tab-select-tab-dwim)))

;;;; Hide Cursor

(use-package hide-cursor
  :bind ("<f8>" . hide-cursor-mode))

;;; Application and utilities

;;;; Command-line shells

(use-package password-cache
  :custom
  (password-cache t)
  (password-cache-expiry 600))

;; Eshell extensions
(use-package ut-eshell
  :commands
  ut-eshell-clear-scrollback
  ut-eshell-last-dir)

;; Shell implemented in elisp
(use-package eshell
  :custom
  (eshell-modules-list
   '(eshell-alias
     eshell-banner
     eshell-basic
     eshell-cmpl
     eshell-dirs
     eshell-extpipe
     eshell-glob
     eshell-hist
     eshell-ls
     eshell-pred
     eshell-prompt
     eshell-script
     eshell-term
     eshell-unix
     eshell-tramp))
  (eshell-prefer-lisp-function t)
  (eshell-cd-on-directory t)
  :config
  (setenv "PAGER" "cat"))

(use-package esh-mode
  :bind ( :map eshell-mode-map
	  ("M-k" . eshell-kill-input)
	  ("C-c M-o" . ut-eshell-clear-scrollback)))

(use-package em-hist
  :bind ( :map eshell-hist-mode-map
	  ("M-s" . nil)
	  ("C-c C-r" . ut-eshell-last-dir)))

(use-package shell
  :bind (("s-S-<return>" . shell))
  :custom
  (shell-command-prompt-show-cwd t))

(use-package vterm
  :bind (("s-<return>" . ut/vterm))
  :config
  (defun ut/vterm (&optional arg)
    "Switch to vterm buffer based on current project."
    (interactive "P")
    (if (project-current)
	(project-vterm arg)
      (vterm arg))))

;;;; Dictionary

(use-package dictionary
  :bind (("C-h M" . dictionary-search))
  :custom
  (dictionary-server nil))

;;;; Process monitor

;; built in process viewer inside emacs
(use-package proced
  :hook ((proced-mode . hl-line-mode))
  :custom
  (proced-auto-update-flag t)
  (proced-auto-update-interval 5)
  (proced-descend t)
  (proced-filter 'user))

;;;; Email

(use-package auth-source
  :custom
  (auth-sources '(password-store))
  (user-full-name "Utkarsh Singh")
  (user-mail-address "utkarsh190601@gmail.com"))

(use-package message
  :init
  (setq message-directory (file-name-concat (xdg-data-home) "mail/"))
  :custom
  (mail-user-agent 'message-user-agent)
  (message-mail-user-agent nil)
  (message-kill-buffer-on-exit t)
  (message-signature "Utkarsh Singh\nhttps://utkarshsingh.xyz/\n")
  (mail-signature "Utkarsh Singh\nhttps://utkarshsingh.xyz\n")
  (message-citation-line-format "On %Y-%m-%d, %R %z, %f wrote:\n")
  (message-citation-line-function 'message-insert-formatted-citation-line)
  (message-confirm-send nil)
  (message-kill-buffer-on-exit t)
  (message-wide-reply-confirm-recipients t)
  :config
  (defun ut/message-quote-region (beg0 end0 &optional arg)
    "Quote message in region BEG and END.
With optional ARG prefix argument (\\[universal-argument]) toggle
quote character to TAB instead of '>'."
    (interactive "r\nP")
    (let ((beg (save-excursion
		 (goto-char (min beg0 end0))
		 (point)))
	  (end (save-excursion
		 (goto-char (max beg0 end0))
		 (end-of-line 1)
		 (point-marker)))
	  (quote (if arg "\t" "> ")))
      (goto-char beg)
      (while (< (point) end)
	(insert quote)
	(forward-line 1)))))

(use-package gnus-dired
  :hook ((dired-mode . gnus-dired-mode)))

(use-package mm-encode
  :custom
  (mm-encrypt-option nil) ; use 'guided if you need more control
  (mm-sign-option nil))   ; same

(use-package mml
  :custom
  (mml-attach-file-at-the-end t))

(use-package mml-sec
  :custom
  (mml-secure-openpgp-encrypt-to-self t)
  (mml-secure-openpgp-sign-with-sender t)
  (mml-secure-smime-encrypt-to-self t)
  (mml-secure-smime-sign-with-sender t))

;; send mail from inside emacs using smtp protocol
(use-package smtpmail
  :custom
  (sendmail-program "msmtp")
  (send-mail-function 'smtpmail-send-it)
  (message-sendmail-f-is-evil t)
  (message-sendmail-extra-arguments '("--read-envelope-from"))
  (message-send-mail-function 'message-send-mail-with-sendmail))

;; manage mail with powerful search features and effective tag sorting
(use-package notmuch
  :bind (("C-x m" . notmuch-mua-new-mail)
	 :map notmuch-search-mode-map
	 ("a" . nil)
	 ("A" . notmuch-search-archive-thread))
  :hook ((notmuch-message-mode . (lambda () (corfu-mode -1))))
  :commands
  notmuch
  :custom
  (notmuch-fcc-dirs
   '(("utkarsh190601@gmail.com" . "utkarsh190601@gmail.com/[Gmail].Sent +sent -inbox -unread")))

  ;; UI setting
  (notmuch-show-logo nil)
  (notmuch-hello-sections '(notmuch-hello-insert-header
			    notmuch-hello-insert-saved-searches
			    notmuch-hello-insert-footer))
  (notmuch-column-control t)

  (notmuch-archive-tags '("-inbox" "-unread" "+deleted"))
  (notmuch-draft-tags '("-inbox" "-unread" "-sent" "+draft"))
  (notmuch-crypto-process-mime t)

  ;; Search
  (notmuch-saved-searches
   `(( :name "inbox"
       :query "tag:inbox"
       :sort-order newest-first
       :key ,(kbd "i"))
     ( :name "unread"
       :query "tag:unread and tag:inbox"
       :sort-order newest-first
       :key ,(kbd "u"))
     ( :name "unread all"
       :query "tag:unread"
       :sort-order newest-first
       :key ,(kbd "U"))
     ( :name "linkedin"
       :query "tag:linkedin"
       :sort-order newest-first
       :key ,(kbd "l"))
     ( :name "hdfc"
       :query "tag:hdfc"
       :sort-order newest-first
       :key ,(kbd "h"))
     ( :name "market"
       :query "tag:nse or tag:bse or tag:cdsl"
       :sort-order newest-first
       :key ,(kbd "m")))))

(use-package ut-notmuch)

;;;; Feed Reader

;; Elfeed extension
(use-package ut-elfeed
  :bind ( :map elfeed-search-mode-map
	  ("v" . ut-elfeed-mpv-dwim)
	  :map elfeed-show-mode-map
	  ("v" . ut-elfeed-mpv-dwim)
	  ("e" . ut-elfeed-show-eww))
  :commands ut-elfeed-load-feeds)

;; RSS and Atom feed reader
(use-package elfeed
  :ensure t
  :hook ((elfeed-show-mode . (lambda () (setq-local shr-width (current-fill-column))))
	 (elfeed-search-mode . ut-elfeed-load-feeds))
  :custom
  (elfeed-use-curl t)
  (elfeed-curl-max-connections 10)
  (elfeed-db-directory (locate-user-emacs-file "elfeed/"))
  (elfeed-enclosure-default-dir "~/Downloads/")
  (elfeed-search-clipboard-type 'CLIPBOARD)
  (elfeed-show-truncate-long-urls t)
  (elfeed-show-unique-buffers t))

;;;; Password manager

(use-package password-store
  :ensure t)

;;;; Version control tools

(use-package vc
  :hook ((log-view-mode . hl-line-mode))
  :custom
  (vc-follow-symlinks t)
  (vc-git-diff-switches '("--patch-with-stat"))
  (vc-git-log-switches '("--date=iso")))

;; extension for VC
(use-package ut-vc
  :bind ( :map vc-git-log-edit-mode-map
	  ("C-C C-n" . ut-vc-git-log-edit-extract-file-name))
  :commands ut-vc-git-setup-mode
  :config
  (ut-vc-git-setup-mode 1))

;; Git porcelain
(use-package magit
  :ensure t
  :bind (("C-c M-g" . nil)
	 ("C-c g" . magit-file-dispatch)
	 :map magit-status-mode-map
	 ("C-<tab>" . nil))
  :custom
  (git-commit-summary-max-length 50)
  (git-commit-style-convention-checks '(non-empty-second-line
					overlong-summary-line))
  (magit-diff-refine-hunk nil)
  (magit-repository-directories '(("~/Documents/project" . 1)
				  ("~/.local/src" . 1))))

;;;; Org Mode

(use-package org
  :bind (("C-c l" . org-store-link)
	 ("C-c a" . org-agenda)
	 ("C-c c" . org-capture)
	 :map org-mode-map
	 ("C-<return>" . nil)
	 ("C-S-<return>" . nil))
  :init
  (setq org-directory (file-name-concat (xdg-data-home) "org"))
  (setq org-default-notes-file (file-name-concat (xdg-data-home) "notes"))
  (setq org-publish-timestamp-directory (file-name-concat (xdg-data-home) "org-timestamps"))
  (setq org-export-backends '(ascii html latex odt texinfo))
  :custom
  (org-adapt-indentation nil)
  (org-catch-invisible-edits 'show)
  (org-log-done 'time)

  ;; source code
  (org-src-preserve-indentation t)
  (org-edit-src-content-indentation 0)

  ;; HTML Export settings
  (org-html-doctype "html5")
  (org-html-html5-fancy t)
  (org-html-head-include-default-style nil)
  (org-html-head-include-scripts nil)
  (org-html-postamble nil))

;; CSV library
(use-package pcsv
  :ensure t)

;; edit time table in plain text
(use-package ttable)

;;;; Emacs Web Wowser (eww)

;; HTML parser
(use-package shr
  :custom
  (shr-max-width 100)
  (shr-width nil)                  ; check `ut-eww-readable'
  (shr-use-colors nil)
  (shr-use-fonts nil)
  (shr-max-image-proportion 0.6))

(use-package eww
  :custom
  (eww-header-line-format nil)
  ;; (browse-url-browser-function 'eww)
  (eww-auto-rename-buffer 'title)
  (eww-download-directory (expand-file-name "~/Downloads/eww")))

(use-package ut-eww
  :bind-keymap (("C-x w" . ut-eww-map))
  :bind ( :map dired-mode-map
	  ("E" . ut-eww-dired-open-files)
	  :map ut-eww-map
	  ("b" . ut-eww-visit-bookmark)
	  ("e" . ut-eww-browse-dwim)
	  ("s" . ut-eww-search-engine)
	  :map eww-mode-map
	  ("R" . ut-eww-readable))
  :config
  (define-prefix-command 'ut-eww-map))

;;;; IRC Client

(use-package rcirc
  :after password-store
  :custom
  (rcirc-server-alist
   `(("irc.libera.chat"
      :channels ("#emacs")
      :port 6697 :encryption tls
      :password ,(password-store-get "libera/utkarshsingh"))))

  (rcirc-default-nick "utkarshsingh")
  (rcirc-default-user-name rcirc-default-nick)
  (rcirc-default-full-name "Utkarsh Singh"))

;;;; Remote File Access

(use-package tramp)

;;;; Note Taking

(use-package denote
  :ensure t
  :custom
  (denote-file-type 'text)
  (denote-directory "~/Documents/notes"))

;;;; Focus mode

(use-package olivetti
  :ensure t
  :custom
  (olivetti-minimum-body-width shr-max-width))

(use-package logos
  :ensure t
  :custom
  (logos-hide-mode-line t)
  (logos-hide-header-line t)
  (logos-hide-buffer-boundaries t)
  (logos-hide-fringe t)
  (logos-olivetti t)
  (logos-variable-pitch t)
  :bind ("<f9>" . logos-focus-mode))

;;;; Spell checker

(use-package jinx
  :ensure t
  :hook (emacs-startup . global-jinx-mode)
  :bind (("M-$" . jinx-correct)
         ("C-M-$" . jinx-languages)))

;;; Settings for prose and programming

;;;; White-spaces, TAB and line numbers

(use-package display-line-numbers
  :bind (("<f7>" . ut/line-number-and-hl-mode))
  :custom
  (display-line-numbers-type t)
  (display-line-numbers-widen t)
  :config
  (define-minor-mode ut/line-number-and-hl-mode
    "Toggle `display-line-numbers-mode' and `hl-line-mode'."
    :init-value nil
    :global nil
    (if ut/line-number-and-hl-mode
        (progn
          (display-line-numbers-mode 1)
          (hl-line-mode 1)
          (setq-local truncate-lines t))
      (display-line-numbers-mode -1)
      (hl-line-mode -1)
      (setq-local truncate-lines nil))))

(use-package whitespace
  :bind (("<f6>" . ut/toggle-invisibles)
	 ("C-c z" . delete-trailing-whitespace))
  :config
  (defun ut/toggle-invisibles ()
    "Toggles the display of indentation and space characters."
    (interactive)
    (if (bound-and-true-p whitespace-mode)
        (whitespace-mode -1)
      (whitespace-mode))))

(use-package ut-fill
  :custom
  (fill-column 70)
  :config
  (ut-fill-fill-mode 1))

(use-package emacs
  :custom
  (tab-always-indent 'complete))

;;;; Parentheses

;; auto-pair
(use-package electric
  :config
  (electric-pair-mode 1))

;; parentheses highlighting
(use-package paren
  :config
  (show-paren-mode 1))

;;;; Documentation

(use-package help
  ;; Use C-h o
  :bind (("C-h f" . nil)
	 ("C-h v" . nil))
  :custom
  (describe-bindings-outline t))

(use-package shortdoc
  :bind ( :map shortdoc-mode-map
	  ("C-c C-n" . nil)
	  ("C-c C-p" . nil)
	  ("M-n" . shortdoc-next-section)
	  ("M-p" . shortdoc-previous-section)))

(use-package man
  :custom
  (Man-notify-method 'pushy))

;; elisp live documentation feedback
(use-package eldoc
  :custom
  (eldoc-echo-area-use-multiline-p nil)
  :config
  (global-eldoc-mode 1))

;;;; Programming utility

;; Incremental parsing library
(use-package treesit
  :custom
  (major-mode-remap-alist '((python-mode . python-ts-mode)))
  :config
  (add-to-list 'auto-mode-alist '("\\.ts\\'" . typescript-ts-mode))
  (add-to-list 'auto-mode-alist '("\\.yaml\\'" . yaml-ts-mode))
  (add-to-list 'auto-mode-alist '("\\.rs\\'" . rust-ts-mode))
  (add-to-list 'major-mode-remap-alist '(c-mode . c-ts-mode))
  (add-to-list 'major-mode-remap-alist '(c++-mode . c++-ts-mode))
  (add-to-list 'major-mode-remap-alist '(c-or-c++-mode . c-or-c++-ts-mode)))

(use-package newcomment
  :bind (("C-;" . comment-dwim))
  :custom
  (comment-style 'indent))

;; Extensions to newcomment
(use-package ut-comment
  :bind (("M-;" . ut-comment-timestamp-keyword)))

;; Language server mode
(use-package eglot
  :custom
  (eglot-autoshutdown t)
  :bind (("C-c a" . eglot-code-actions)
	 ("C-c j" . eglot-format)
	 ("C-c r" . eglot-rename))
  :hook ((python-ts-mode . eglot-ensure)
	 (typescript-ts-mode . eglot-ensure)
	 (rust-ts-mode . eglot-ensure)
	 (c-ts-mode . eglot-ensure)
	 (c++-ts-mode . eglot-ensure)
	 (c-or-c++-ts-mode . eglot-ensure)))

;; Syntax checker
(use-package flymake
  :bind ( :map flymake-mode-map
	  ("M-n" . flymake-goto-next-error)
	  ("M-p" . flymake-goto-prev-error)))

;; Code formatter
(use-package apheleia
  :ensure t
  :hook ((python-ts-mode . apheleia-mode)
	 (js-mode . apheleia-mode)
	 (typescript-ts-mode . apheleia-mode)
	 (html-mode . apheleia-mode)
	 (yaml-ts-mode . apheleia-mode)
	 (rust-ts-mode . apheleia-mode)
	 (css-mode . apheleia-mode)
	 (c-ts-mode . apheleia-mode)
	 (c++-ts-mode . apheleia-mode)
	 (c-or-c++-ts-mode . apheleia-mode))
  :config
  (setf (alist-get 'python-ts-mode apheleia-mode-alist)
      '(isort black)))

(use-package editorconfig
  :ensure t
  :config
  (editorconfig-mode 1))

;;; Languages

;;;; C, C++, Objective-C and Java

(use-package cc-mode
  :custom
  (c-default-style '((awk-mode . "awk")
		     (c++-mode . "stroustrup")
		     (java-mode . "java")
		     (other . "gnu"))))

;;;; Python

(use-package python
  :custom
  (python-indent-offset 4))

(use-package pyvenv
  :ensure t)

;;;; Scheme, Guile and Racket

(use-package geiser
  :ensure t
  :custom
  (geiser-repl-history-filename (locate-user-emacs-file "geiser/history"))
  (geiser-repl-buffer-name-function #'ut/geiser-repl-buffer-name)
  :config
  (defun ut/geiser-repl-buffer-name (impl)
    "Return default name of the REPL buffer for implementation IMPL."
    (format "*%s*" (geiser-repl--repl-name impl))))

(use-package geiser-guile
  :ensure t)

(use-package geiser-racket
  :ensure t)

;;;; Common Lisp

(use-package sly
  :ensure t
  :custom
  (inferior-lisp-program "sbcl"))

;;;; TeX

(use-package tex-mode
  :bind ( :map tex-mode-map
	  ("C-<return>" . nil)))

(use-package tex
  :ensure auctex
  :custom
  (TeX-auto-save t)
  (TeX-parse-self t))

;;;; Assembly

(use-package asm-mode
  :custom
  (asm-comment-char ?\#))

;;;; Markdown

(use-package markdown-mode
  :ensure t)

;;;; Arduino

(use-package arduino-mode
  :custom
  (arduino-custom-lib-dir (file-name-concat (xdg-data-home) "arduino/libraries")))

;;;; Web

(use-package js
  :custom
  (js-indent-level 2))

(use-package nodejs-repl
  :hook ((js-mode . nodejs-repl-minor-mode)))


;;; History and state

;;;; Emacs server

;; to start emacs server
(use-package server
  :hook ((after-init . ut/server-start))
  :config
  (defun ut/server-start ()
    (unless (server-running-p)
      (server-start))))

;;;; Record history

(use-package savehist
  :custom
  (history-delete-duplicate t)
  :config
  (savehist-mode 1))

;; built-in minor mode that keeps track of the files
;; you have opened, allowing you revisit them faster.
(use-package recentf
  :custom
  (recentf-max-menu-items 25)
  (recentf-max-saved-items 25)
  :config
  (recentf-mode 1))

(use-package ut-recentf
  :bind (("C-x C-r" . ut-recentf-recent-files))
  :config
  (add-to-list 'recentf-keep 'ut-recentf-keep-predicate))

(use-package saveplace
  :config
  (save-place-mode 1))

(use-package saveplace-pdf-view
  :ensure t)

(use-package emacs
  :custom
  (backup-directory-alist `(("." . ,(locate-user-emacs-file "backup/"))))
  (delete-old-version t))

(provide 'init)

;; Local Variables:
;; outline-regexp: ";;;+"
;; eval: (outline-minor-mode 1)
;; End:

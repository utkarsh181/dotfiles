;;; early-init.el --- Early init file -*- lexical-binding: t -*-

;; Copyright (c) 2020, 2021, 2022, 2023 Utkarsh Singh <utkarsh190601@gmail.com>

;; Author: Utkarsh Singh <utkarsh190601@gmail.com>
;; Package-Requires: ((emacs "28.1"))

;; This file is NOT part of GNU Emacs.

;; This file is free software: you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by the
;; Free Software Foundation, either version 3 of the License, or (at
;; your option) any later version.
;;
;; This file is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; We use this file to suppress that automatic behaviour so that
;; startup is consistent across Emacs versions.

;;; Code:

;; Initialise installed packages
(setq package-enable-at-startup t)

;; Quickstart packages
(setq package-quickstart t)

;; Do not resize the frame at this early stage.
(setq frame-inhibit-implied-resize t)

(setq native-comp-async-report-warnings-errors 'silent)

(setq native-compile-prune-cache t)

;; Disable GUI elements
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(setq inhibit-splash-screen t)

;; So we can detect this having been loaded
(provide 'early-init)

;;; early-init.el ends here
